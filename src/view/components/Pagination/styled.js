import styled from 'styled-components';
import Svg from 'src/view/components/Svg';

export const PaginationLink = styled.li`
  height: 42px;
  width: 42px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 4px;
  border-radius: 100%;
  background: #ca2b21;
  opacity: 0.6;

  ${(props) =>
    props.active &&
    `
    opacity: 1;
    `}
  &:hover {
    opacity: 1;
  }
`;

export const PaginationWrapper = styled.ul`
  display: flex;
  flex-wrap: wrap;
  position: relative;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  padding: 0;
  padding-left: 48px;
  padding-right: 48px;
`;

export const PaginationArrow = styled(Svg)`
  cursor: pointer;
  position: absolute;
  width: 48px;
  height: 48px;
`;

export const RightPaginationArrow = styled(PaginationArrow)`
  right: 0;
`;

export const LeftPaginationArrow = styled(PaginationArrow)`
  left: 0;
`;
