export const TEST_IDS = {
  WRAPPER: 'footer',
  LOGO: 'footer__logo',
  NAVIGATION: 'footer__navigation',
  BADGES: 'footer__badges',
  BADGE_ANDROID: 'footer__badge--android',
  BADGE_IOS: 'footer__badge--ios',
  COPYRIGHT: 'footer__copyright'
};
