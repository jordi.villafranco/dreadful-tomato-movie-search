import styled from 'styled-components';
import { breakpoints } from 'src/constants';
import Text from 'src/view/components/Text';

export const Wrapper = styled.div`
  width: 100%;
  padding-left: 24px;
  padding-right: 24px;

  @media ${breakpoints.desktop} {
    max-width: 1280px;
  }
`;

export const ResultsTitle = styled(Text)`
  display: block;
  margin-top: 24px;

  @media ${breakpoints.tablet} {
    margin-top: 48px;
  }
`;
