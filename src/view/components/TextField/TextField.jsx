import React from 'react';
import { TEST_IDS } from './constants';
import { Wrapper, Icon, Input, Label } from './styled';

function TextField({ icon = 'search', label, name, ...props }) {
  const renderIcon = () => {
    return icon && <Icon icon={icon} data-testid={TEST_IDS.ICON} />;
  };
  const renderLabel = () => {
    return (
      label && (
        <Label data-testid={TEST_IDS.LABEL} htmlFor={name}>
          {label}
        </Label>
      )
    );
  };
  return (
    <Wrapper data-testid={TEST_IDS.WRAPPER}>
      {renderIcon()}
      {renderLabel()}
      <Input name={name} aria-label={label} id={name} {...props} />
    </Wrapper>
  );
}

export default TextField;
