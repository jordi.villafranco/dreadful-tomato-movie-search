import { Then, When } from 'cypress-cucumber-preprocessor/steps';
import ResultsFilterFeature from './resultsFilterFeature';

When('I filter the results by the title {string}', (title) => {
  ResultsFilterFeature.filterByTitle(title);
});

When('I filter the results by the date {string}', (date) => {
  ResultsFilterFeature.filterByDate(date);
});

Then(
  'I see the expected results for the title filter {string}',
  (titleFilter) => {
    ResultsFilterFeature.assertResultsByTitleFilter(titleFilter);
  }
);

Then(
  'I see the expected results for the date filter {string}',
  (dateFilter) => {
    ResultsFilterFeature.assertResultsByDateFilter(dateFilter);
  }
);
