import React from 'react';
import Datetime from 'react-datetime';
import { Wrapper, DatepickerTextField } from './styled';

function Datepicker({ onChange, ...props }) {
  return (
    <Wrapper aria-hidden={true}>
      <Datetime
        renderInput={(props, openCalendar) => (
          <DatepickerTextField
            onClick={openCalendar}
            autoComplete="off"
            {...props}
          />
        )}
        dateFormat="YYYY"
        timeFormat={false}
        onChange={(date) => onChange(date)}
        {...props}
      />
    </Wrapper>
  );
}

export default Datepicker;
