import { MOVIES_TYPE } from 'src/constants';
import { MOVIES_TYPE_FILTER, SERIES_TYPE_FILTER } from './constants';

export const getTypeFilterFromPathname = (pathname) => {
  if (pathname === `/${MOVIES_TYPE}`) {
    return MOVIES_TYPE_FILTER;
  }
  return SERIES_TYPE_FILTER;
};

export const queryTitle = (title, query) => {
  if (query) {
    return title.toLowerCase().startsWith(query.toLowerCase());
  }
  return true;
};

export const queryDate = (date, query) => {
  if (query) {
    return date + '' === query + '';
  }
  return true;
};
