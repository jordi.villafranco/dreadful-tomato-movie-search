import { TEST_IDS, TITLE_TEXT_SERIES, TITLE_TEXT_MOVIES } from '../constants';
import { MOVIES_TYPE, SERIES_TYPE } from 'src/constants';
import { getDataTestIdByType, getTitleTextByType } from '../collaborators';

describe('Pages - Results - Collaborators', () => {
  describe('getTitleTextByType function', () => {
    it('returns the series page title when type is series', () => {
      const result = getTitleTextByType(SERIES_TYPE);
      expect(result).toBe(TITLE_TEXT_SERIES);
    });
    it('returns the movies page title when type is movies', () => {
      const result = getTitleTextByType(MOVIES_TYPE);
      expect(result).toBe(TITLE_TEXT_MOVIES);
    });
    it('returns the series page title when type is unknown', () => {
      const result = getTitleTextByType('test');
      expect(result).toBe(TITLE_TEXT_SERIES);
    });
    it('returns an empty string when type parameter is not received', () => {
      const result = getTitleTextByType();
      expect(result).toBe('');
    });
  });

  describe('getDataTestIdByType function', () => {
    it('returns the series data-testid when type is series', () => {
      const result = getDataTestIdByType(SERIES_TYPE);
      expect(result).toBe(TEST_IDS.SERIES_RESULTS);
    });
    it('returns the movies data-testid when type is movies', () => {
      const result = getDataTestIdByType(MOVIES_TYPE);
      expect(result).toBe(TEST_IDS.MOVIES_RESULTS);
    });
    it('returns the series data-testid when type is unknown', () => {
      const result = getDataTestIdByType('test');
      expect(result).toBe(TEST_IDS.SERIES_RESULTS);
    });
  });
});
