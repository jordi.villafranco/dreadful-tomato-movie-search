import { render, screen } from '@testing-library/react';
import { TEST_IDS } from './constants';
import Footer from './Footer';

const renderFooter = (props) => render(<Footer {...props} />);

describe('Container - Footer', () => {
  beforeEach(() => {
    renderFooter();
  });

  it('renders footer container', () => {
    const footer = screen.getByTestId(TEST_IDS.WRAPPER);
    expect(footer).toBeInTheDocument();
  });

  it('should redirect to the root page when logo link is clicked', () => {
    const logo = screen.getByTestId(TEST_IDS.LOGO);
    expect(logo.closest('a')).toHaveAttribute('href', '/');
  });
});
