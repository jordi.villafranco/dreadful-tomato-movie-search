Feature: User is able to choose whether to access SERIES or movies information

  As a User I want to see the services offered
  so I can choose one of them
  and be redirected to the corresponding page

  Scenario Outline: User can navigate to the desired page
    Given I visit the "landing" page
    And I see the "<section>" section
    When I click in the "<section>" section
    Then I am redirected to the "<section>" page

    Examples:
      | section |
      | movies  |
      | series  |