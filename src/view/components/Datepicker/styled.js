import styled from 'styled-components';
import TextField from 'src/view/components/TextField';
import { breakpoints } from 'src/constants';
import 'react-datetime/css/react-datetime.css';

export const Wrapper = styled.div`
  .rdtPicker {
    width: 100%;
    margin-top: 8px;
    @media ${breakpoints.desktop} {
      min-width: 0 !important;
    }
  }
  .rdtPicker .rdtActive {
    background-color: #ca2b21 !important;
  }
`;

export const DatepickerTextField = styled(TextField)`
  min-width: 250px;
  cursor: pointer;
  caret-color: transparent;
`;
