import { queryTitle, queryDate, getTypeFilterFromPathname } from '../utils';
import { MOVIES_TYPE, SERIES_TYPE } from 'src/constants';
import { MOVIES_TYPE_FILTER, SERIES_TYPE_FILTER } from '../constants';

describe('Testing utils', () => {
  describe('Testing getTypeFilterFromPathname function', () => {
    it('should return the MOVIES type filter when it receives the MOVIES pathname', () => {
      expect(getTypeFilterFromPathname(`/${MOVIES_TYPE}`)).toBe(
        MOVIES_TYPE_FILTER
      );
    });

    it('should return the SERIES type filter when it receives the SERIES pathname', () => {
      expect(getTypeFilterFromPathname(`/${SERIES_TYPE}`)).toBe(
        SERIES_TYPE_FILTER
      );
    });

    it('should return the SERIES type filter when it does not receive pathname', () => {
      expect(getTypeFilterFromPathname()).toBe(SERIES_TYPE_FILTER);
    });

    it('should return the SERIES type filter when it receives an unknown pathname', () => {
      expect(getTypeFilterFromPathname('')).toBe(SERIES_TYPE_FILTER);
    });
  });

  describe('Testing queryTitle function', () => {
    it('should return true if no query is received', () => {
      expect(queryTitle('title')).toBe(true);
    });

    it('should return true if title starts with the query', () => {
      const titleMock = 'title';
      const queryMock = 'ti';
      expect(queryTitle(titleMock, queryMock)).toBe(true);
    });

    it('should return true if title starts with the query regardless of casing', () => {
      const titleMock = 'title';
      const queryMock = 'TI';
      expect(queryTitle(titleMock, queryMock)).toBe(true);
    });

    it('should return false if title contains query in the middle', () => {
      const titleMock = 'title';
      const queryMock = 'tl';
      expect(queryTitle(titleMock, queryMock)).toBe(false);
    });

    it('should return false if title does not contain query', () => {
      const titleMock = 'title';
      const queryMock = 'query';
      expect(queryTitle(titleMock, queryMock)).toBe(false);
    });
  });

  describe('Testing queryDate function', () => {
    it('should return true if no query is received', () => {
      expect(queryDate('date')).toBe(true);
    });

    it('should return true if date is equal to query', () => {
      const dateMock = '2015';
      const queryMock = '2015';
      expect(queryDate(dateMock, queryMock)).toBe(true);
    });

    it('should return false if date is not equal to query', () => {
      const dateMock = '2015';
      const queryMock = '2020';
      expect(queryDate(dateMock, queryMock)).toBe(false);
    });
  });
});
