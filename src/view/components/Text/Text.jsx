/** This component implements IBM Productive type set from carbon styled system
 *
 * https://www.carbondesignsystem.com/guidelines/typography/productive
 */

import React from 'react';
import {
  BodyShort02,
  ProductiveHeading02,
  ProductiveHeading03,
  ProductiveHeading04,
  HelperText01
} from './styled';

function Text({ styling = 'body-short-02', fontWeight, children, ...props }) {
  const renderComponentByStyle = {
    'helper-text-01': <HelperText01 {...props}>{children}</HelperText01>,
    'body-short-02': <BodyShort02 {...props}>{children}</BodyShort02>,
    'productive-heading-02': (
      <ProductiveHeading02 {...props}>{children}</ProductiveHeading02>
    ),
    'productive-heading-03': (
      <ProductiveHeading03 {...props}>{children}</ProductiveHeading03>
    ),
    'productive-heading-04': (
      <ProductiveHeading04 {...props}>{children}</ProductiveHeading04>
    )
  };
  return renderComponentByStyle[styling];
}

export default Text;
