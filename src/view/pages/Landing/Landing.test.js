import { render, screen } from '@testing-library/react';
import Landing from './Landing';
import { TEST_IDS } from './constants';

const renderLandingPage = () => render(<Landing />);

describe('Page - Landing page', () => {
  beforeEach(() => {
    renderLandingPage();
  });

  it('renders the series section', () => {
    const wrapper = screen.getByTestId(TEST_IDS.WRAPPER);
    const seriesSection = screen.getByTestId(TEST_IDS.SERIES_SECTION);
    expect(wrapper).toContainElement(seriesSection);
  });

  it('renders the movie section', () => {
    const wrapper = screen.getByTestId(TEST_IDS.WRAPPER);
    const moviesSection = screen.getByTestId(TEST_IDS.MOVIES_SECTION);
    expect(wrapper).toContainElement(moviesSection);
  });
});
