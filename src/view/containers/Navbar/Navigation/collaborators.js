import { SERIES_TYPE, MOVIES_TYPE } from 'src/constants';
import { MOVIES_NAVLINK_HREF } from './constants';

export const isNavLinkActive = (navLinkType, currentPathname) => {
  if (navLinkType === SERIES_TYPE && currentPathname !== MOVIES_NAVLINK_HREF) {
    return true;
  }
  if (navLinkType === MOVIES_TYPE && currentPathname === MOVIES_NAVLINK_HREF) {
    return true;
  }
  return false;
};
