import styled from 'styled-components';
import { breakpoints } from 'src/constants';
import Logo from 'src/view/components/Logo';

export const Nav = styled.nav`
  width: 100%;
  min-height: 64px;
  padding-left: 24px;
  padding-right: 24px;
  z-index: 1;
  top: 0;

  display: flex;
  align-items: stretch;
  justify-content: center;
  flex-direction: row;
  overflow: hidden;
  background: black;

  & span {
    font-weight: 600;
  }
`;

export const ContentWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;

  flex-direction: row;
  max-width: 1280px;
  padding: 24px;
  flex-grow: 1;
  flex-wrap: wrap;

  @media ${breakpoints.tablet} {
    justify-content: flex-start;
    padding: 0;
  }
`;

export const ButtonsWrapper = styled.div`
  display: flex;
  @media ${breakpoints.tablet} {
    flex-grow: 1;
    justify-content: flex-end;
  }
`;

export const NavbarLogo = styled(Logo)`
  margin-bottom: 8px;
  padding-left: 156px;
  padding-right: 156px;

  @media ${breakpoints.tablet} {
    padding: 0;
    width: auto;
    margin-bottom: 0;
    margin-right: 24px;
  }
`;
export const NavButton = styled.button`
  display: none;
  cursor: pointer;
  border-radius: 50px;
  padding: 8px;
  padding-left: 16px;
  padding-right: 16px;
  background: #ca2b21;
  align-self: center;
  align-items: center;

  @media ${breakpoints.desktop} {
    display: block;
    margin-right: 8px;
    margin-left: 16px;
    justify-content: center;
    padding-left: 16px;
    padding-right: 16px;
  }
`;
