import styled, { css } from 'styled-components';
import Text from 'src/view/components/Text';
import Link from 'src/view/components/Link';

import { breakpoints } from 'src/constants';

export const Wrapper = styled.ul`
  display: flex;
  @media ${breakpoints.tablet} {
    flex-grow: 1;
  }
`;

export const NavLinkActive = css`
  background-color: #ca2b21;
  text-decoration: none;
`;

export const Navlink = styled(Link)`
  display: flex;
  align-items: center;
  padding: 8px;
  border-radius: 4px;
  margin-right: 4px;
  margin-left: 4px;
  ${(props) =>
    props.active &&
    `
    ${NavLinkActive}
  `}

  &:hover {
    ${NavLinkActive}
  }

  @media ${breakpoints.tablet} {
    padding: 0;
    margin: 0;
    border-radius: 0;
    padding-left: 16px;
    padding-right: 16px;
  }
`;

export const NavlinkText = styled(Text)`
  margin-left: 8px;
  display: none;

  @media ${breakpoints.desktop} {
    display: block;
  }
`;
