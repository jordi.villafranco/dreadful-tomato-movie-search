import styled from 'styled-components';
import { breakpoints } from 'src/constants';

export const Wrapper = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  padding: 24px;
  padding-left: 0;
  padding-right: 0;

  @media ${breakpoints.tablet} {
    padding: 48px;
  }
`;
