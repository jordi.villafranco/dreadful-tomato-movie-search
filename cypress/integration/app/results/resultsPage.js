const selectors = {
  TITLE_SECTION: 'results__title',
  MOVIES_RESULTS: 'results--movies',
  SERIES_RESULTS: 'results--series',
  CARD: 'resultsList__card',
  CARD_INFORMATION: 'card__information',
  SERIES_NAVLINK: 'navbar__navlink--series',
  MOVIES_NAVLINK: 'navbar__navlink--movies',
  FILTER_TOGGLER: 'navlink__filter-toggler',
  FILTERS: 'filters'
};

const selectResultsContainerByResultsType = {
  movies: selectors['MOVIES_RESULTS'],
  series: selectors['SERIES_RESULTS']
};

const selectTitleContentByResultsType = {
  movies: 'movies',
  series: 'series'
};

const selectNavLinkByDestinationPage = {
  movies: selectors.MOVIES_NAVLINK,
  series: selectors.SERIES_NAVLINK
};

class ResultsPage {
  static assertResultsAsVisible(resultsType) {
    cy.getByTestId(selectResultsContainerByResultsType[resultsType]).should(
      'be.visible'
    );
  }

  static assertNumberOfResultsInRange() {
    cy.getByTestId(selectors['CARD']).its('length').should('be.gte', 0);
    cy.getByTestId(selectors['CARD']).its('length').should('be.lte', 10);
  }

  static assertFiltersTogglerAsVisible() {
    cy.getByTestId(selectors['FILTER_TOGGLER']).should('be.visible');
  }

  static assertFiltersAsVisible() {
    cy.getByTestId(selectors['FILTERS']).should('be.visible');
  }

  static assertFiltersAsHidden() {
    cy.getByTestId(selectors['FILTERS']).should('not.exist');
  }

  static clickFiltersToggler() {
    cy.getByTestId(selectors['FILTER_TOGGLER']).click();
  }

  static hoverResultCard() {
    cy.getByTestId(selectors['CARD']).eq(0).trigger('mouseover');
    cy.getByTestId(selectors['CARD_INFORMATION']).invoke('show');
  }

  static assertCardInformationAsVisible() {
    cy.getByTestId(selectors['CARD_INFORMATION']).eq(0).should('be.visible');
  }

  static assertTitleAsVisible() {
    cy.getByTestId(selectors['TITLE_SECTION']).should('be.visible');
  }

  static assertTitleAsTruthy(resultsType) {
    cy.getByTestId(selectors['TITLE_SECTION']).contains(
      selectTitleContentByResultsType[resultsType],
      {
        matchCase: false
      }
    );
  }

  static clickNavigationLinkByPage(page) {
    cy.getByTestId(selectNavLinkByDestinationPage[page]).click();
  }
}

export default ResultsPage;
