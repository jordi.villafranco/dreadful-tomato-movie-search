import styled from 'styled-components';

export const Background = styled.div`
  width: 100%;
  height: fill;
  background-image: url(${(props) => props.backgroundImage});
  background-position: center;
  background-size: cover;
`;

export const Footer = styled.div`
  position: absolute;
  bottom: 0;
  display: flex;
  align-items: center;
  padding: 24px;
  width: 100%;
  opacity: 75%;
  background-color: #ca2b21;
`;

export const Wrapper = styled.a`
  position: relative;
  cursor: pointer;
  opacity: 75%;

  &:hover {
    opacity: 100%;
  }

  &:hover ${Footer} {
    opacity: 100%;
  }
`;
