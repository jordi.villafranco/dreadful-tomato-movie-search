import { isNavLinkActive } from '../collaborators';
import { SERIES_TYPE, MOVIES_TYPE } from 'src/constants';
import { SERIES_NAVLINK_HREF, MOVIES_NAVLINK_HREF } from '../constants';

describe('Testing collaborators', () => {
  describe('Testing isNavLinkActive function', () => {
    it('should return true when nav type is series regardless of pathname', () => {
      expect(isNavLinkActive(SERIES_TYPE, SERIES_NAVLINK_HREF)).toBe(true);
      expect(isNavLinkActive(SERIES_TYPE, 'pathname')).toBe(true);
      expect(isNavLinkActive(SERIES_TYPE)).toBe(true);
    });

    it('should return true when nav type is movie and current pathname is movie', () => {
      expect(isNavLinkActive(MOVIES_TYPE, MOVIES_NAVLINK_HREF)).toBe(true);
    });

    it('should return false when nav type is movie and current pathname is series', () => {
      expect(isNavLinkActive(MOVIES_TYPE, SERIES_NAVLINK_HREF)).toBe(false);
    });

    it('should return false when nav type is series and current pathname is movies', () => {
      expect(isNavLinkActive(SERIES_TYPE, MOVIES_NAVLINK_HREF)).toBe(false);
    });

    it('should return false when nav type is not series and no current pathname is received', () => {
      expect(isNavLinkActive('type')).toBe(false);
      expect(isNavLinkActive(MOVIES_TYPE)).toBe(false);
    });

    it('should return false when no arguments are received', () => {
      expect(isNavLinkActive()).toBe(false);
    });
  });
});
