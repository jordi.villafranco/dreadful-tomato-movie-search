import bgMovies from 'src/assets/movies.png';
import bgSeries from 'src/assets/series.png';

export const TEST_IDS = {
  WRAPPER: 'sectionBanner',
  BACKGROUND: 'sectionBanner__background',
  TITLE: 'sectionBanner__title'
};

const constants = {
  SERIES_BG: bgSeries,
  MOVIES_BG: bgMovies,
  SERIES_REDIRECTION: '/series',
  MOVIES_REDIRECTION: '/movies',
  MOVIES_TEXT: 'Movies',
  SERIES_TEXT: 'Series'
};

export default constants;
