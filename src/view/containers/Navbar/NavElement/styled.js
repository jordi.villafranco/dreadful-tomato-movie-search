import styled, { css } from 'styled-components';
import { breakpoints } from 'src/constants';
import Text from 'src/view/components/Text';

const StyledNavElementActive = css`
  background: #202020;
  border-bottom: 3px solid #ca2b21;
`;

export const StyledNavElement = styled.button`
  display: flex;
  cursor: pointer;
  margin-right: 4px;
  margin-left: 4px;
  align-items: center;
  padding: 8px;
  border-radius: 4px;
  flex-direction: ${(props) => props.flexDirection};

  img {
    width: 31px;
    height: 31px;
  }

  ${(props) =>
    props.active &&
    `
    ${StyledNavElementActive}
  `}

  &:hover {
    ${StyledNavElementActive}
  }

  @media ${breakpoints.tablet} {
    border-radius: 0px;
  }

  @media ${breakpoints.desktop} {
    justify-content: center;
    padding: 0;
    margin: 0;
    padding-left: 16px;
    padding-right: 16px;

    img {
      width: auto;
      height: auto;
    }
  }
`;

export const NavElementText = styled(Text)`
  margin-right: 8px;
  margin-left: 8px;
  font-weight: 500;
  display: none;

  @media ${breakpoints.desktop} {
    display: block;
  }
`;
