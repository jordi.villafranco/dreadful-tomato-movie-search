import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import GlobalStyle from 'src/view/styles';
import history from 'src/utils/history';

import Landing from './Landing';
import Results from './Results';

function Root() {
  return (
    <>
      <GlobalStyle />
      <ConnectedRouter history={history}>
        <Switch>
          <Route path="/" component={Landing} exact />
          <Route path="/:type" component={Results} />
        </Switch>
      </ConnectedRouter>
    </>
  );
}

export default Root;
