import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import Pagination from 'src/view/components/Pagination';
import { selectors as resultsSelectors } from 'src/state/results';
import { selectors as routeSelectors } from 'src/state/router';
import location from 'src/utils/location';
import { TEST_IDS } from './constants';
import { Wrapper } from './styled';
import { calculatePageCount } from './collaborators';
import { URL_PARAM_PAGE } from 'src/constants';

function ResultsPagination() {
  const entriesNumber = useSelector(resultsSelectors.selectNumberOfEntries);
  const resultsPerPage = useSelector(resultsSelectors.selectResultsPerPage);
  const currentPage = useSelector(routeSelectors.selectPageQuery);

  const [pageCount, setPageCount] = useState(1);

  useEffect(() => {
    setPageCount(calculatePageCount(entriesNumber, resultsPerPage));
  }, [entriesNumber, resultsPerPage]);

  const handlePaginationChange = (newPage) => {
    location.addQuery({ [URL_PARAM_PAGE]: newPage });
  };

  return (
    <Wrapper data-testid={TEST_IDS.PAGINATION} as="nav">
      <Pagination
        currentPage={currentPage}
        count={pageCount}
        onChange={(newPage) => handlePaginationChange(newPage)}
      />
    </Wrapper>
  );
}

export default ResultsPagination;
