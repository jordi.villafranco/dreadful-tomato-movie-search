import { And, When } from 'cypress-cucumber-preprocessor/steps';
import LandingPage from './LandingPage';

And('I see the {string} section', (section) => {
  LandingPage.assertSectionAsVisible(section);
});

When('I click in the {string} section', (section) => {
  LandingPage.clickSection(section);
});
