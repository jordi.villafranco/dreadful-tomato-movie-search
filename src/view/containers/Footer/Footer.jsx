import React from 'react';
import androidBadge from 'src/assets/google-play.png';
import iosBadge from 'src/assets/app-store.png';
import { TEST_IDS } from './constants';
import {
  StyledFooter,
  Logo,
  Navigation,
  NavigationLink,
  BadgesContainer,
  Badge,
  CopyrightContainer,
  CopyrightText
} from './styled';

function Footer() {
  return (
    <StyledFooter data-testid={TEST_IDS.WRAPPER}>
      <Logo data-testid={TEST_IDS.LOGO} />
      <Navigation data-testid={TEST_IDS.NAVIGATION}>
        <NavigationLink href="/">Home</NavigationLink>
        <NavigationLink href="/">Terms of Use</NavigationLink>
        <NavigationLink href="/">Legal Notices</NavigationLink>
        <NavigationLink href="/">Help</NavigationLink>
        <NavigationLink href="/">Manage Account</NavigationLink>
      </Navigation>
      <BadgesContainer data-testid={TEST_IDS.BADGES}>
        <Badge
          src={iosBadge}
          data-testid={TEST_IDS.BADGE_IOS}
          alt="Go to IOS App Store"
        />
        <Badge
          src={androidBadge}
          data-testid={TEST_IDS.BADGE_ANDROID}
          alt="Go to Android play store"
        />
      </BadgesContainer>
      <CopyrightContainer data-testid={TEST_IDS.COPYRIGHT}>
        <CopyrightText styling="helper-text-01">
          Copyright 2020 Dreadful Tomato Streaming All Rights Reserved
        </CopyrightText>
      </CopyrightContainer>
    </StyledFooter>
  );
}

export default Footer;
