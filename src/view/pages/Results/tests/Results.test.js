import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Route } from 'react-router-dom';
import Results from '../Results';
import { TEST_IDS, TITLE_TEXT_SERIES, TITLE_TEXT_MOVIES } from '../constants';
import store from 'src/state/store';
import { MOVIES_TYPE, SERIES_TYPE } from 'src/constants';

const renderResultsPage = (type = SERIES_TYPE) =>
  render(
    <Provider store={store}>
      <MemoryRouter initialEntries={[`/${type}`]}>
        <Route path="/:type" component={Results} />
      </MemoryRouter>
    </Provider>
  );

describe('Page - Results page', () => {
  describe('rendering of series results', () => {
    beforeEach(() => {
      renderResultsPage(SERIES_TYPE);
    });
    it('renders the series title if the url type param is series', () => {
      const wrapper = screen.getByTestId(TEST_IDS.WRAPPER);
      const title = screen.getByTestId(TEST_IDS.TITLE);
      const results = screen.getByTestId(TEST_IDS.SERIES_RESULTS);
      expect(wrapper).toContainElement(title);
      expect(wrapper).toContainElement(results);

      expect(title.textContent).toContain(TITLE_TEXT_SERIES);
    });
  });

  describe('rendering of movies results', () => {
    beforeEach(() => {
      renderResultsPage(MOVIES_TYPE);
    });
    it('renders the movies results if the url type param is movies', () => {
      const wrapper = screen.getByTestId(TEST_IDS.WRAPPER);
      const results = screen.getByTestId(TEST_IDS.MOVIES_RESULTS);
      const title = screen.getByTestId(TEST_IDS.TITLE);

      expect(wrapper).toContainElement(results);
      expect(wrapper).toContainElement(title);
      expect(title.textContent).toContain(TITLE_TEXT_MOVIES);
    });
  });

  describe('filters toggling', () => {
    beforeEach(() => {
      renderResultsPage();
    });

    it('renders the filters container by default', () => {
      const wrapper = screen.getByTestId(TEST_IDS.WRAPPER);
      const filters = screen.getByTestId(TEST_IDS.FILTER_CONTAINER);

      expect(wrapper).toContainElement(filters);
    });

    it('hides the filters container after clicking on filter toggler', () => {
      const wrapper = screen.getByTestId(TEST_IDS.WRAPPER);
      const filters = screen.getByTestId(TEST_IDS.FILTER_CONTAINER);
      const filtersToggler = screen.getByTestId(TEST_IDS.FILTER_TOGGLER);

      expect(wrapper).toContainElement(filters);
      fireEvent.click(filtersToggler);
      expect(filters).not.toBeInTheDocument();
    });
  });
});
