import { render, screen, within } from '@testing-library/react';
import Card from './Card';
import { TEST_IDS } from './constants';

const renderCard = (props) => render(<Card {...props} />);

describe('Component - Card', () => {
  it('renders the received title', () => {
    const titleMock = 'title';
    renderCard({ title: titleMock });
    const card = screen.getByTestId(TEST_IDS.CARD);
    const { getByText } = within(card);

    expect(getByText(titleMock)).toBeInTheDocument();
  });

  it('renders the received description', () => {
    const descriptionMock = 'description';
    renderCard({ description: descriptionMock });
    const card = screen.getByTestId(TEST_IDS.CARD);
    const { getByText } = within(card);

    expect(getByText(descriptionMock)).toBeInTheDocument();
  });

  it('renders the received date', () => {
    const dateMock = '2023';
    renderCard({ date: dateMock });
    const card = screen.getByTestId(TEST_IDS.CARD);
    const { getByText } = within(card);

    expect(getByText(dateMock)).toBeInTheDocument();
  });

  it('renders the received background image', () => {
    const backgroundImageMock = '/validbg';
    renderCard({ backgroundImage: backgroundImageMock });
    const card = screen.getByTestId(TEST_IDS.CARD);

    expect(card).toHaveStyle(`background-image: url(${backgroundImageMock})`);
  });
});
