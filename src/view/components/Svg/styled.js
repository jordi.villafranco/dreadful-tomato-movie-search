import styled from 'styled-components';

export const StyledSvg = styled.svg`
  height: 24px;
  width: 24px;
  fill: white;
`;
