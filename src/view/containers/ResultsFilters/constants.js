export const TEST_IDS = {
  FILTERS: 'filters',
  FILTERS_TITLE: 'filters__input--title',
  FILTERS_TITLE_FORM: 'filters__form--title',
  FILTERS_DATEPICKER: 'filters__input--datepicker',
  FILTERS_DATEPICKER_FORM: 'filters__form--datepicker'
};

const constants = {
  TITLE_INPUT_PLACEHOLDER: 'Name',
  TITLE_INPUT_NAME: 'input_title',
  DATEPICKER_PLACEHOLDER: '1920',
  DATEPICKER_INPUT_NAME: 'input_datepicker'
};

export default constants;
