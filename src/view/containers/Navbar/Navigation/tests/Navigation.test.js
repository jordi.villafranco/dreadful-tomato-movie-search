import { fireEvent, render, screen, within } from '@testing-library/react';
import { TEST_IDS } from '../../constants';
import location from 'src/utils/location';
import {
  SERIES_NAVLINK_TEXT,
  SERIES_NAVLINK_HREF,
  SERIES_NAVLINK_IMGSRC,
  MOVIES_NAVLINK_TEXT,
  MOVIES_NAVLINK_HREF,
  MOVIES_NAVLINK_IMGSRC
} from '../constants';
import Navigation from '../Navigation';

const renderNavigation = (props) => render(<Navigation {...props} />);

describe('Container - Navigation', () => {
  beforeEach(() => {
    renderNavigation();
    location.push = jest.fn();
  });

  it('renders navigation component', () => {
    const navigation = screen.getByTestId(TEST_IDS.NAVIGATION);
    expect(navigation).toBeInTheDocument();
  });

  describe('series navigation link', () => {
    it('renders the navigation link', () => {
      const navigation = screen.getByTestId(TEST_IDS.NAVIGATION);
      const seriesLink = screen.getByTestId(TEST_IDS.SERIES_NAVLINK);
      expect(navigation).toContainElement(seriesLink);
    });

    it('renders the correct content for series navigation link', () => {
      const seriesLink = screen.getByTestId(TEST_IDS.SERIES_NAVLINK);
      const text = within(seriesLink).getByTestId(TEST_IDS.NAVLINK_TEXT);
      const icon = within(seriesLink).getByTestId(TEST_IDS.NAVLINK_IMG);

      expect(text.textContent).toContain(SERIES_NAVLINK_TEXT);
      expect(icon).toHaveAttribute('src', SERIES_NAVLINK_IMGSRC);
    });

    it('updates location to series pathname onClick', () => {
      const seriesLink = screen.getByTestId(TEST_IDS.SERIES_NAVLINK);
      fireEvent.click(seriesLink);
      expect(location.push).toHaveBeenCalledWith(SERIES_NAVLINK_HREF);
    });
  });

  describe('movies navigation link', () => {
    it('renders the movies navigation link', () => {
      const navigation = screen.getByTestId(TEST_IDS.NAVIGATION);
      const moviesLink = screen.getByTestId(TEST_IDS.MOVIES_NAVLINK);
      expect(navigation).toContainElement(moviesLink);
    });
    it('renders the correct content for movies navigation link', () => {
      const moviesLink = screen.getByTestId(TEST_IDS.MOVIES_NAVLINK);
      const text = within(moviesLink).getByTestId(TEST_IDS.NAVLINK_TEXT);
      const icon = within(moviesLink).getByTestId(TEST_IDS.NAVLINK_IMG);

      expect(text.textContent).toContain(MOVIES_NAVLINK_TEXT);
      expect(icon).toHaveAttribute('src', MOVIES_NAVLINK_IMGSRC);
    });
    it('updates location to series pathname onClick', () => {
      const moviesLink = screen.getByTestId(TEST_IDS.MOVIES_NAVLINK);
      fireEvent.click(moviesLink);
      expect(location.push).toHaveBeenCalledWith(MOVIES_NAVLINK_HREF);
    });
  });
});
