import React from 'react';
import { LogoImg, StyledAnchor } from './styled';
import logo from 'src/assets/logo.png';
import { ALT_TEXT, TEST_IDS } from './constants';

function Logo({ ...props }) {
  return (
    <StyledAnchor data-testid={TEST_IDS.LOGO_ANCHOR} href="/">
      <LogoImg src={logo} alt={ALT_TEXT} {...props} />
    </StyledAnchor>
  );
}

export default Logo;
