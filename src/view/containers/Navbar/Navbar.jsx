import React from 'react';
import Text from 'src/view/components/Text';
import {
  TRIAL_TEXT,
  LOGIN_NAVLINK_TEXT,
  LOGIN_NAVLINK_IMGSRC,
  FILTERS_NAVLINK_IMGSRC,
  FILTERS_NAVLINK_TEXT,
  TEST_IDS
} from './constants';
import NavElement from './NavElement';

import Navigation from './Navigation';
import {
  Nav,
  ContentWrapper,
  NavbarLogo,
  NavButton,
  ButtonsWrapper
} from './styled';

function Navbar({
  hasNavigation,
  hasFilterToggler,
  isFiltersTogglerActive,
  handleFilterTogglerClick
}) {
  return (
    <Nav data-testid={TEST_IDS.WRAPPER}>
      <ContentWrapper>
        <NavbarLogo data-testid={TEST_IDS.LOGO} />
        {hasNavigation && <Navigation data-testid={TEST_IDS.NAVIGATION} />}
        <ButtonsWrapper>
          {hasFilterToggler && (
            <NavElement
              text={FILTERS_NAVLINK_TEXT}
              image={FILTERS_NAVLINK_IMGSRC}
              imageAlt="Filters icon"
              data-testid={TEST_IDS.FILTER_TOGGLER}
              active={isFiltersTogglerActive}
              onClick={handleFilterTogglerClick}
              flexDirection="row-reverse"
            />
          )}
          <NavElement
            text={LOGIN_NAVLINK_TEXT}
            image={LOGIN_NAVLINK_IMGSRC}
            imageAlt="Login icon"
            data-testid={TEST_IDS.LOGIN}
          />
          <NavButton data-testid={TEST_IDS.FREE_TRIAL} role="button">
            <Text data-testid={TEST_IDS.NAVLINK_TEXT}>{TRIAL_TEXT}</Text>
          </NavButton>
        </ButtonsWrapper>
      </ContentWrapper>
    </Nav>
  );
}

export default Navbar;
