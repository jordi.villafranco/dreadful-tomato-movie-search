import styled from 'styled-components';

const Layout = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #141414;
  width: 100%;
  min-height: 100vh;
`;

export default Layout;
