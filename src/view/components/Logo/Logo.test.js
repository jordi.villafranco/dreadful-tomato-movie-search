import { render, screen } from '@testing-library/react';
import Logo from './Logo';
import { TEST_IDS } from './constants';

const renderLogo = (props) => render(<Logo {...props} />);

describe('Component - Logo', () => {
  beforeEach(() => {
    renderLogo();
  });
  it('renders the component', () => {
    const anchor = screen.getByTestId(TEST_IDS.LOGO_ANCHOR);
    expect(anchor).toBeInTheDocument();
  });
  it('redirects to root onClick', () => {
    const anchor = screen.getByTestId(TEST_IDS.LOGO_ANCHOR);
    expect(anchor).toHaveAttribute('href', '/');
    expect(anchor).toBeInTheDocument();
  });
});
