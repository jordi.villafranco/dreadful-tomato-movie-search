import styled from 'styled-components';
import CardComponent from 'src/view/components/Card';
import { breakpoints } from 'src/constants';

export const Wrapper = styled.main`
  flex-grow: 1;
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  margin-top: 16px;

  @media ${breakpoints.desktop} {
    padding: 0;
  }
`;

export const StyledCard = styled(CardComponent)`
  flex: 1 1 100%;
  margin: 8px;
  margin-left: 0px;
  margin-right: 0px;

  @media ${breakpoints.tablet} {
    flex: 1 1 calc(20% - 8px);
    margin: 4px;
  }
`;
