import styled from 'styled-components';
import { breakpoints } from 'src/constants';
import SectionBanner from 'src/view/containers/SectionBanner';

export const Wrapper = styled.main`
  width: 100%;
  flex-grow: 1;
  display: flex;
  flex-direction: column;

  @media ${breakpoints.tablet} {
    flex-direction: row;
  }
`;

export const StyledSectionBanner = styled(SectionBanner)`
  min-height: 500px;
  display: flex;

  @media ${breakpoints.tablet} {
    flex: 1 1 50%;
  }
`;
