import React from 'react';
import Layout from 'src/view/components/Layout';
import Footer from 'src/view/containers/Footer';
import Navbar from 'src/view/containers/Navbar';
import { TEST_IDS } from './constants';
import { MOVIES_TYPE, SERIES_TYPE } from 'src/constants';
import { Wrapper, StyledSectionBanner } from './styled';

function Landing() {
  return (
    <Layout>
      <Navbar />
      <Wrapper data-testid={TEST_IDS.WRAPPER}>
        <StyledSectionBanner
          data-testid={TEST_IDS.MOVIES_SECTION}
          type={MOVIES_TYPE}
        />
        <StyledSectionBanner
          data-testid={TEST_IDS.SERIES_SECTION}
          type={SERIES_TYPE}
        />
      </Wrapper>
      <Footer />
    </Layout>
  );
}

export default Landing;
