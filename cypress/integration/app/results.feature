Feature: User is able to see the tv shows and movies result pages

  As a User I want to see the tv shows result page
  so I can see the available tv shows

  As a User I want to see the movies result page
  so I can see the available movies

  As a User I want to see a title section
  so I know what kind of results I'm getting

  As a User I want to hover in a result tv show/movie
  so I can a description and release date

  As a User I want to see a pagination section
  so I know I can navigate through the results

  As a User I want to see a filters section
  so I can filter my results

  As a User I want to hide the filters section
  so I can better see my results

  As a User I want to see navigation links
  so I can navigate between pages

  Scenario: User sees the tv shows page
    Given I visit the "series" page
    Then I see the "series" results

  Scenario: User sees the movies page
    Given I visit the "movies" page
    Then I see the "movies" results

  Scenario Outline: User sees the corresponding title section
    Given I visit the "<page>" page
    Then I see the "<page>" title section

    Examples:
      | page   |
      | series |
      | movies |

  Scenario Outline: User sees a max of 10 results per page
    Given I visit the "<page>" page
    Then I see from 0 to 10 results

    Examples:
      | page   |
      | series |
      | movies |

  Scenario Outline: User can see more information about results
    Given I visit the "<page>" page
    When I hover in a result card
    Then I see the result information

    Examples:
      | page   |
      | series |
      | movies |

  Scenario Outline: User can navigate by clicking the navigation links
    Given I visit the "<currentPage>" page
    When I click in the "<destinationPage>" navigation link
    Then I am redirected to the "<destinationPage>" page

    Examples:
      | currentPage | destinationPage |
      | series      | series          |
      | movies      | movies          |

  Scenario Outline: User can navigate to movies page clicking movies navigation link
    Given I visit the "<page>" page
    When I click in the "movies" navigation link
    Then I am redirected to the "movies" page

    Examples:
      | page   |
      | series |
      | movies |

  Scenario Outline: User sees the filter section
    Given I visit the "<page>" page
    Then I can see the filters section

    Examples:
      | page   |
      | series |
      | movies |

  Scenario Outline: User hides the filter section
    Given I visit the "<page>" page
    And I see the filters toggler
    When I click in the filters toggler
    Then I do not see the filters section

    Examples:
      | page   |
      | series |
      | movies |


