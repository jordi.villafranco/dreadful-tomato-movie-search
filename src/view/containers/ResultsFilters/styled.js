import styled from 'styled-components';
import { breakpoints } from 'src/constants';

export const FullWidthWrapper = styled.div`
  background: #ca2b21;
  width: 100vw;
  padding: 16px;
  margin-left: calc(50% - 50vw);
  margin-right: calc(50% - 50vw);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 1280px;
  width: 100%;
  @media ${breakpoints.tablet} {
    flex-direction: row;
  }
`;

export const TitleInputForm = styled.form`
  display: flex;
  flex: 1 1 100%;
  margin-bottom: 16px;
  @media ${breakpoints.tablet} {
    margin-right: 16px;
    margin-bottom: 0;
  }
`;

export const DateInputForm = styled.form`
  flex-grow: 0;
`;
