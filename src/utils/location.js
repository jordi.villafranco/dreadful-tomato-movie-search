import history from './history';
import qs from 'query-string';

const location = {
  push(pathname) {
    history.push(pathname);
  },
  addQuery(query) {
    const queryParams = qs.parse(history.location.search);
    const newQueries = { ...queryParams, ...query };
    history.push({ search: qs.stringify(newQueries) });
  }
};

export default location;
