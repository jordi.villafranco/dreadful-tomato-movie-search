import styled from 'styled-components';
import Svg from 'src/view/components/Svg';

export const Wrapper = styled.div`
  width: 100%;
  position: relative;
`;

export const Input = styled.input`
  border-radius: 50px;
  padding: 16px;
  padding-left: 64px;
  border: 0;
  width: 100%;
  font-size: 16px;
  font-weight: 500;
`;

export const Icon = styled(Svg)`
  position: absolute;
  left: 24px;
  background-blend-mode: screen;

  top: 50%;
  transform: translateY(-50%);
  width: 28px;
  height: 28px;
  fill: #ca2b21;

  &:after {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.5);
  }
`;

export const Label = styled.label`
  display: none;
`;
