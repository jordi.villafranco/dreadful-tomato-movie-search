Cypress.Commands.add('getByTestId', (value) => {
  return cy.get(`[data-testid=${value}]`);
});

Cypress.Commands.add('matchFixtureByTestId', (testId, fixture) => {
  cy.fixture(fixture).then((logo) => {
    cy.getByTestId(testId).should(
      'have.attr',
      'src',
      `data:image/png;base64,${logo}`
    );
  });
});
