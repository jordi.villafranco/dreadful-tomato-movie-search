import { configureStore } from '@reduxjs/toolkit';
import { connectRouter } from 'connected-react-router';
import history from 'src/utils/history';
import { reducer as resultsReducer } from './results';

const rootReducer = (history) => ({
  router: connectRouter(history),
  results: resultsReducer
});

const store = configureStore({
  reducer: rootReducer(history)
});

export default store;
