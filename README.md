![Logo](images/logo.png 'Dreadful Tomato')

# Dreadful Tomato

Dreadful Tomato is a new platform to find new movies and TV shows.

The main objective is to help users to find information about their favourite TV shows and movies.

## How to run the project

1. Install dependencies by running `npm install`
2. Run `npm start`
3. Test in http://localhost:3000

## How to run the Acceptance Tests

This project has a exhaustive acceptance test suite implemented with `cypress`. Testing if the core features work should be a breeze!

To run the `cypress` test suite execute the following command in your terminal:

    npm run cy:run

You can have a more visual experience by running the tests manually:

    npm run cy:open

## Features implemented

You can also check for these features in the cypress folder as they have all been documented in Gherkin language (thanks to cucumber preprocessor)

The main features are:

- Single route that can render different results (movies and series)
- Filtering by title and release date
- Pagination
- Filtering and pagination by url search query

## Development notes

### Stack

The project has been developed using the following **stack**

- react
- redux
- react-testing-library
- styled-components

### This project covers:

- Complete react application in ES6
- Managing of state via redux
- Managing of filters via search queries
- Unit testing
- Acceptance testing
- Responsiveness

**It notoriously doesn't cover**
- Fetching from a REST API nor graphql. Currently the data is in a json file. Fetching from a public API would be easy to implement if needed.
- TypeScript development.

### Goals

- Achieve a consistent format in the codebase
  - `eslint`, `prettier` and `husky` (auto-prettier on commit) implemented for this regard
- Orthogonal architecture (DRY isolated code)
- High unit test coverage
- Acceptance test coverage of the main futures (`cypress and cucumber`)
  - Filters Toggler
  - Filtering by queries
  - Pagination
  - Logo is shown in the layout
  - Navigation between pages via navigation bar
  - etc (you can check out the cucumber tests in `/cypress/integration`)

### Development methodology

Used **TDD+BDD** methodology for all features

1. Write cucumber acceptance tests for the feature
2. Write unit tests for one element of the feature
3. Code until unit test is green light
4. Refactor
5. Repeat 2-4 until acceptance tests are green light