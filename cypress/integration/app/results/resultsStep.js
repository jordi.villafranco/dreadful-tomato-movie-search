import { Then, When, And } from 'cypress-cucumber-preprocessor/steps';
import ResultsPage from './ResultsPage';

Then('I see the {string} results', (resultsType) => {
  ResultsPage.assertResultsAsVisible(resultsType);
});

Then('I see from 0 to 10 results', () => {
  ResultsPage.assertNumberOfResultsInRange();
});

And('I see the filters toggler', () => {
  ResultsPage.assertFiltersTogglerAsVisible();
});

When('I click in the filters toggler', () => {
  ResultsPage.clickFiltersToggler();
});

Then('I can see the filters section', () => {
  ResultsPage.assertFiltersAsVisible();
});

Then('I do not see the filters section', () => {
  ResultsPage.assertFiltersAsHidden();
});

When('I hover in a result card', () => {
  ResultsPage.hoverResultCard();
});

Then('I see the result information', () => {
  ResultsPage.assertCardInformationAsVisible();
});

Then('I see the {string} title section', (page) => {
  ResultsPage.assertTitleAsVisible(page);
  ResultsPage.assertTitleAsTruthy(page);
});

When('I click in the {string} navigation link', (destinationPage) => {
  ResultsPage.clickNavigationLinkByPage(destinationPage);
});
