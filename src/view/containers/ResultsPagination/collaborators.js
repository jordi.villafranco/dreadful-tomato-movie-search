export const calculatePageCount = (length = null, resultsPerPage = null) => {
  if (!length || resultsPerPage === null) {
    return 0;
  }
  if (resultsPerPage > length) {
    return 1;
  }
  return Math.ceil(length / resultsPerPage);
};
