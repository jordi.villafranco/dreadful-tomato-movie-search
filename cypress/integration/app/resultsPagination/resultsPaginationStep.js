import { Then, And, When } from 'cypress-cucumber-preprocessor/steps';
import ResultsPagination from './ResultsPaginationFeature';

And('I see the pagination', () => {
  ResultsPagination.assertPaginationAsVisible();
});

And('I click in the pagination link number {int}', (page) => {
  ResultsPagination.clickPaginationLink(page);
});

When('I click in the last pagination link', () => {
  ResultsPagination.clickLastPaginationLink();
});

Then('I see the results of page {int}', (page) => {
  ResultsPagination.assertResultsUrlUpdated(page);
});

And('I see the left arrow', () => {
  ResultsPagination.assertLeftArrowAsVisible();
});

When('I click in the left arrow', () => {
  ResultsPagination.clickLeftArrow();
});

And('I see the right arrow', () => {
  ResultsPagination.assertLeftArrowAsVisible();
});

When('I click in the right arrow', () => {
  ResultsPagination.clickRightArrow();
});

Then('I do not see the left arrow', () => {
  ResultsPagination.assertLeftArrowAsHidden();
});

Then('I do not see the right arrow', () => {
  ResultsPagination.assertRightArrowAsHidden();
});
