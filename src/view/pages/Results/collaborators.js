import { TEST_IDS, TITLE_TEXT_SERIES, TITLE_TEXT_MOVIES } from './constants';
import { MOVIES_TYPE } from 'src/constants';

export const getTitleTextByType = (type) => {
  if (!type) return '';
  if (type === MOVIES_TYPE) {
    return TITLE_TEXT_MOVIES;
  }
  return TITLE_TEXT_SERIES;
};

export const getDataTestIdByType = (type) => {
  if (type === MOVIES_TYPE) {
    return TEST_IDS.MOVIES_RESULTS;
  }
  return TEST_IDS.SERIES_RESULTS;
};
