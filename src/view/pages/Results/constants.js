import { TEST_IDS as NAVBAR_TEST_IDS } from 'src/view/containers/Navbar/constants';

export const TEST_IDS = {
  WRAPPER: 'results',
  MOVIES_RESULTS: 'results--movies',
  SERIES_RESULTS: 'results--series',
  TITLE: 'results__title',
  FILTER_CONTAINER: 'results__filters',
  FILTER_TOGGLER: NAVBAR_TEST_IDS.FILTER_TOGGLER
};

export const TITLE_TEXT_SERIES = 'Popular Series';
export const TITLE_TEXT_MOVIES = 'Popular Movies';
