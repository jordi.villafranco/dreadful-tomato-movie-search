import { URL_PARAM_PAGE, URL_PARAM_TITLE, URL_PARAM_DATE } from 'src/constants';

export const selectPathname = (state) => state.router.location.pathname;
export const selectPageQuery = (state) =>
  state.router.location.query[URL_PARAM_PAGE] || 0;
export const selectTitleQuery = (state) =>
  state.router.location.query[URL_PARAM_TITLE];
export const selectDateQuery = (state) =>
  state.router.location.query[URL_PARAM_DATE];
