import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import * as api from './api';

export const initialState = {
  entries: [],
  resultsPerPage: 10
};

const fetchResults = createAsyncThunk('results/fetchResults', async () => {
  const response = await api.fetchResults();
  return response;
});

const resultsSlice = createSlice({
  name: 'resultsSlice',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchResults.fulfilled, (state, action) => {
      const entries = action.payload.entries;
      state.entries = entries;
    });
  }
});

export const actions = {
  fetchResults,
  ...resultsSlice.actions
};

export const { reducer } = resultsSlice;
