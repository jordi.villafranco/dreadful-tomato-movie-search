import seriesIcon from 'src/assets/icon-series.png';
import moviesIcon from 'src/assets/icon-movies.png';
import { SERIES_TYPE, MOVIES_TYPE } from 'src/constants';

export const SERIES_NAVLINK_TEXT = 'Series';
export const SERIES_NAVLINK_IMGSRC = seriesIcon;
export const SERIES_NAVLINK_HREF = `/${SERIES_TYPE}`;

export const MOVIES_NAVLINK_TEXT = 'Movies';
export const MOVIES_NAVLINK_IMGSRC = moviesIcon;
export const MOVIES_NAVLINK_HREF = `/${MOVIES_TYPE}`;
