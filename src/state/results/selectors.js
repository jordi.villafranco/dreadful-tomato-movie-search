import { createSelector } from '@reduxjs/toolkit';
import { selectors as routerSelectors } from '../router';
import { getTypeFilterFromPathname, queryDate, queryTitle } from './utils';

const getEntries = (state) => state.results.entries;
export const selectResultsPerPage = (state) => state.results.resultsPerPage;

export const selectFilteredResults = createSelector(
  getEntries,
  routerSelectors.selectPathname,
  routerSelectors.selectTitleQuery,
  routerSelectors.selectDateQuery,
  routerSelectors.selectPageQuery,
  (entries, pathname, titleQuery, dateQuery) => {
    const type = getTypeFilterFromPathname(pathname);
    return entries
      .filter((e) => e.programType === type)
      .filter((e) => queryTitle(e.title, titleQuery))
      .filter((e) => queryDate(e.releaseYear, dateQuery));
  }
);

export const selectPaginatedResults = createSelector(
  selectFilteredResults,
  routerSelectors.selectPageQuery,
  selectResultsPerPage,
  (entries, pageQuery, maxResults) => {
    const startResultIndex = maxResults * pageQuery;
    const endResultIndex = maxResults * pageQuery + maxResults;
    return entries.slice(startResultIndex, endResultIndex);
  }
);

export const selectNumberOfEntries = createSelector(
  selectFilteredResults,
  (entries) => entries.length
);
