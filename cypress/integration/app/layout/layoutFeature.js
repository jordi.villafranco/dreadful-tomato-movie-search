const selectors = {
  NAV: 'navbar',
  NAV_LOGO: 'navbar__logo',
  FOOTER: 'footer',
  FOOTER_LOGO: 'footer__logo',
  FOOTER_NAVIGATION: 'footer__navigation',
  FOOTER_COPYRIGHT: 'footer__copyright',
  FOOTER_BADGES: 'footer__badges',
  FOOTER_APP_ANDROID: 'footer__badge--android',
  FOOTER_APP_IOS: 'footer__badge--ios'
};

class LayoutFeature {
  static assertNavbarAsVisible() {
    cy.getByTestId(selectors.NAV).should('be.visible');
  }

  static assertNavLogoIsCompanyLogo() {
    cy.matchFixtureByTestId(selectors.NAV_LOGO, 'images/logo.png');
  }

  static clickNavLogo() {
    cy.getByTestId(selectors.NAV_LOGO).click();
  }

  static assertFooterAsVisible() {
    cy.getByTestId(selectors.FOOTER).should('be.visible');
  }

  static assertFooterLogoIsCompanyLogo() {
    cy.matchFixtureByTestId(selectors.FOOTER_LOGO, 'images/logo.png');
  }

  static clickFooterLogo() {
    cy.getByTestId(selectors.FOOTER_LOGO).click();
  }

  static assertFooterAppBadgesAsVisible() {
    cy.getByTestId(selectors.FOOTER_APP_ANDROID).should('be.visible');
    cy.getByTestId(selectors.FOOTER_APP_IOS).should('be.visible');
  }

  static assertFooterAppBadgesAsTruthy() {
    cy.matchFixtureByTestId(
      selectors.FOOTER_APP_ANDROID,
      'images/google-play.png'
    );
    cy.matchFixtureByTestId(selectors.FOOTER_APP_IOS, 'images/app-store.png');
  }

  static assertFooterNavigationAsVisible() {
    cy.getByTestId(selectors.FOOTER_NAVIGATION).should('be.visible');
  }

  static assertFooterCopyrightAsVisible() {
    cy.getByTestId(selectors.FOOTER_COPYRIGHT).should('be.visible');
  }
}

export default LayoutFeature;
