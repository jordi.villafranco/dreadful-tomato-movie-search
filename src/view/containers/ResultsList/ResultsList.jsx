import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actions, selectors as resultsSelectors } from 'src/state/results';
import { Wrapper, StyledCard } from './styled';
import { TEST_IDS } from './constants';

function ResultsList({ type, ...props }) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actions.fetchResults());
  }, [dispatch]);

  const results = useSelector(resultsSelectors.selectPaginatedResults);

  return (
    <Wrapper data-testid={TEST_IDS.WRAPPER} role="main" {...props}>
      {results.map((entry, i) => (
        <StyledCard
          data-testid={TEST_IDS.CARD}
          key={`${entry.title}-${i}`}
          description={entry.description}
          title={entry.title}
          date={entry.releaseYear}
          backgroundImage={entry.images['Poster Art'].url}
        />
      ))}
    </Wrapper>
  );
}

export default ResultsList;
