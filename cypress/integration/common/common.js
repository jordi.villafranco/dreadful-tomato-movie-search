import { defineStep } from 'cypress-cucumber-preprocessor/steps';
import { LOCALHOST, PATHNAMES } from './constants';

defineStep('I visit the {string} page', (page) => {
  cy.visit(`${LOCALHOST}/${PATHNAMES[page]}`);
});

defineStep('I am redirected to the {string} page', (page) => {
  cy.url().should((givenUrl) => {
    expect(givenUrl.toLowerCase()).to.include(PATHNAMES[page]);
  });
});
