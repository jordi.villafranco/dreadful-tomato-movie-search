import styled from 'styled-components';

export const Text = styled.span`
  color: white;
  user-select: text;
`;

export const HelperText01 = styled(Text)`
  font-size: 12px;
  line-height: 16px;
  font-weight: 400;
  letter-spacing: 0.32px;
`;

export const BodyShort02 = styled(Text)`
  font-size: 16px;
  line-height: 22px;
  font-weight: 400;
  letter-spacing: 0px;
`;

export const ProductiveHeading02 = styled(Text)`
  font-size: 16px;
  line-height: 22px;
  font-weight: 600;
  letter-spacing: 0px;
`;

export const ProductiveHeading03 = styled(Text)`
  font-size: 20px;
  line-height: 28px;
  font-weight: 600;
  letter-spacing: 2px;
`;

export const ProductiveHeading04 = styled(Text)`
  font-size: 28px;
  line-height: 36px;
  font-weight: 600;
  letter-spacing: 2px;
`;
