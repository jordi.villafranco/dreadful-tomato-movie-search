import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { TEST_IDS } from './constants';
import { Wrapper, ResultsTitle } from './styled';
import { getTitleTextByType, getDataTestIdByType } from './collaborators';
import Layout from 'src/view/components/Layout';

import ResultsList from 'src/view/containers/ResultsList';
import Navbar from 'src/view/containers/Navbar';
import Footer from 'src/view/containers/Footer';
import ResultsPagination from 'src/view/containers/ResultsPagination';
import ResultsFilter from 'src/view/containers/ResultsFilters';

function Results() {
  const { type } = useParams();
  const [isFiltersShown, setIsFiltersShown] = useState(true);

  return (
    <Layout>
      <Navbar
        hasFilterToggler
        hasNavigation
        isFiltersTogglerActive={isFiltersShown}
        handleFilterTogglerClick={() => setIsFiltersShown(!isFiltersShown)}
      />
      <Wrapper data-testid={TEST_IDS.WRAPPER}>
        {isFiltersShown && (
          <ResultsFilter data-testid={TEST_IDS.FILTER_CONTAINER} />
        )}
        <ResultsTitle
          forwardedAs="h1"
          tabIndex={0}
          styling="productive-heading-04"
          data-testid={TEST_IDS.TITLE}
        >
          {getTitleTextByType(type)}
        </ResultsTitle>
        <ResultsList data-testid={getDataTestIdByType(type)} />
        <ResultsPagination />
      </Wrapper>
      <Footer />
    </Layout>
  );
}

export default Results;
