import history from '../history';
import location from '../location';

describe('Utility - Location client', () => {
  const mockInitialSearch = '?existing=search';

  beforeEach(() => {
    const url = 'http://dummy.com';
    history.location = {
      href: url,
      search: mockInitialSearch
    };
  });
  describe('push method', () => {
    it('should redirect to the pathname recieved as argument', () => {
      const mockPath = '/mock';

      location.push(mockPath);
      expect(history.location.pathname).toEqual(mockPath);
    });
  });

  describe('addQuery method', () => {
    it('should add the received query to location search', () => {
      const mockQueryKey = 'key';
      const mockQueryValue = 'value';
      const mockQuery = { [mockQueryKey]: mockQueryValue };

      location.addQuery(mockQuery);
      expect(history.location.search).toContain(
        `${mockQueryKey}=${mockQueryValue}`
      );
    });

    it('should maintain the initial search when adding new parameters', () => {
      const mockQueryKey = 'key';
      const mockQueryValue = 'value';
      const mockQuery = { [mockQueryKey]: mockQueryValue };

      location.addQuery(mockQuery);
      expect(history.location.search).toContain(mockInitialSearch);
      expect(history.location.search).toContain(
        `${mockQueryKey}=${mockQueryValue}`
      );
    });
  });
});
