import { Then, And, When } from 'cypress-cucumber-preprocessor/steps';
import LayoutFeature from './LayoutFeature';

Then('I see the navbar', () => {
  LayoutFeature.assertNavbarAsVisible();
});

And('I see the company logo inside the navbar', () => {
  LayoutFeature.assertNavLogoIsCompanyLogo();
});

When('I click in the company logo inside the navbar', () => {
  LayoutFeature.clickNavLogo();
});

Then('I see the footer', () => {
  LayoutFeature.assertFooterAsVisible();
});

And('I see the company logo inside the footer', () => {
  LayoutFeature.assertFooterLogoIsCompanyLogo();
});

When('I click in the company logo inside the footer', () => {
  LayoutFeature.clickFooterLogo();
});

Then('I see the footer app badges', () => {
  LayoutFeature.assertFooterAppBadgesAsVisible();
  LayoutFeature.assertFooterAppBadgesAsTruthy();
});

Then('I see the footer navigation', () => {
  LayoutFeature.assertFooterNavigationAsVisible();
});

Then('I see the footer copyright', () => {
  LayoutFeature.assertFooterCopyrightAsVisible();
});
