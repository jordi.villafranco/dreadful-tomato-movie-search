import React from 'react';
import location from 'src/utils/location';
import { SERIES_TYPE, MOVIES_TYPE } from 'src/constants';
import { Wrapper, Navlink, NavlinkText } from './styled';
import { TEST_IDS } from '../constants';
import {
  SERIES_NAVLINK_TEXT,
  SERIES_NAVLINK_HREF,
  SERIES_NAVLINK_IMGSRC,
  MOVIES_NAVLINK_TEXT,
  MOVIES_NAVLINK_HREF,
  MOVIES_NAVLINK_IMGSRC
} from './constants';
import { isNavLinkActive } from './collaborators';
import { useHistory } from 'react-router';

function Navigation({ ...props }) {
  const history = useHistory();
  const currentPathname = history?.location?.pathname || '';

  const handleLinkClick = (e, pathname) => {
    e.preventDefault();
    location.push(pathname);
  };

  return (
    <Wrapper data-testid={TEST_IDS.NAVIGATION} role="menu" {...props}>
      <Navlink
        data-testid={TEST_IDS.MOVIES_NAVLINK}
        href={MOVIES_NAVLINK_HREF}
        role="menuitem"
        active={isNavLinkActive(MOVIES_TYPE, currentPathname)}
        onClick={(e) => handleLinkClick(e, MOVIES_NAVLINK_HREF)}
      >
        <img
          src={MOVIES_NAVLINK_IMGSRC}
          data-testid={TEST_IDS.NAVLINK_IMG}
          alt="Movie icon"
        />
        <NavlinkText data-testid={TEST_IDS.NAVLINK_TEXT}>
          {MOVIES_NAVLINK_TEXT}
        </NavlinkText>
      </Navlink>
      <Navlink
        data-testid={TEST_IDS.SERIES_NAVLINK}
        active={isNavLinkActive(SERIES_TYPE, currentPathname)}
        href={SERIES_NAVLINK_HREF}
        role="menuitem"
        onClick={(e) => handleLinkClick(e, SERIES_NAVLINK_HREF)}
      >
        <img
          src={SERIES_NAVLINK_IMGSRC}
          data-testid={TEST_IDS.NAVLINK_IMG}
          alt="Series icon"
        />
        <NavlinkText data-testid={TEST_IDS.NAVLINK_TEXT}>
          {SERIES_NAVLINK_TEXT}
        </NavlinkText>
      </Navlink>
    </Wrapper>
  );
}

export default Navigation;
