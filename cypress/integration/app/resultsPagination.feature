Feature: User is able to filter the results in the result page

  As a User I want to see a pagination section
  so I know I can navigate through the results

  As a User I want to click in the pagination links
  to filter my results

  As a User I want to click in the next page arrow
  to navigate to the next page

  As a User I want to click in the previous page arrow
  to navigate to the previous page

  Scenario Outline: User sees the navigation section
    Given I visit the "<page>" page
    Then I see the pagination

    Examples:
      | page   |
      | series |
      | movies |

  Scenario Outline: User can navigate using the pagination links
    Given I visit the "<page>" page
    Then I see the pagination
    And I click in the pagination link number 2
    Then I see the results of page 2

    Examples:
      | page   |
      | series |
      | movies |

  Scenario Outline: User can not click left arrow in page 0
    Given I visit the "<page>" page
    And I see the pagination
    When I click in the pagination link number 0
    Then I do not see the left arrow

    Examples:
      | page   |
      | series |
      | movies |
  
  Scenario Outline: User can not click right arrow in last page
    Given I visit the "<page>" page
    And I see the pagination
    When I click in the last pagination link
    Then I do not see the right arrow

    Examples:
      | page   |
      | series |
      | movies |

  Scenario Outline: User can navigate to previous page using left arrow
    Given I visit the "<page>" page
    And I see the pagination
    And I click in the pagination link number 2
    And I see the left arrow
    When I click in the left arrow
    Then I see the results of page 1

    Examples:
      | page   |
      | series |
      | movies |

  Scenario Outline: User can navigate to next page using right arrow
    Given I visit the "<page>" page
    And I see the pagination
    And I click in the pagination link number 2
    And I see the right arrow
    When I click in the right arrow
    Then I see the results of page 3

    Examples:
      | page   |
      | series |
      | movies |
