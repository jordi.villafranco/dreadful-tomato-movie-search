import styled from 'styled-components';
import DreadfulLogo from 'src/view/components/Logo';
import Text from 'src/view/components/Text';
import Link from 'src/view/components/Link';
import { breakpoints } from 'src/constants';

export const StyledFooter = styled.footer`
  padding: 24px;
  display: flex;
  flex-direction: column;
  background: black;
  width: 100%;
  align-items: center;
`;

export const Logo = styled(DreadfulLogo)`
  width: 180px;
`;

export const Navigation = styled.nav`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 12px;
  padding-bottom: 24px;

  @media ${breakpoints.tablet} {
    flex-direction: row;
  }
`;

export const NavigationLink = styled(Link)`
  margin-right: 8px;
  margin-left: 8px;
  text-align: center;
`;

NavigationLink.defaultProps = {
  forwardedAs: 'a'
};

export const BadgesContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  margin-bottom: 8px;
  @media ${breakpoints.tablet} {
    flex-direction: row;
    margin-bottom: 16px;
  }
`;

export const Badge = styled.img`
  margin-bottom: 8px;
  @media ${breakpoints.tablet} {
    margin-bottom: 0;
    margin-right: 8px;
    margin-left: 8px;
  }
`;

export const CopyrightContainer = styled.div`
  margin-top: 8px;
`;

export const CopyrightText = styled(Text)`
  text-align: center;
  display: block;
  color: grey;
`;
