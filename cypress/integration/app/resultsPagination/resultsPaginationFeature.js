const selectors = {
  PAGINATION: 'pagination',
  PAGINATION_LINK: 'pagination__link',
  PAGINATION_ARROW_RIGHT: 'pagination__arrow--right',
  PAGINATION_ARROW_LEFT: 'pagination__arrow--left'
};

class ResultsPaginationFeature {
  static clickPaginationLink(pageNumber) {
    cy.getByTestId(selectors['PAGINATION_LINK']).eq(pageNumber).click();
  }

  static clickLastPaginationLink() {
    cy.getByTestId(selectors['PAGINATION_LINK']).last().click();
  }

  static assertResultsUrlUpdated(pageNumber) {
    cy.url().should('include', `page=${pageNumber}`);
  }

  static assertPaginationAsVisible() {
    cy.getByTestId(selectors['PAGINATION']).should('be.visible');
    cy.getByTestId(selectors['PAGINATION_LINK']).should('be.visible');
  }

  static assertLeftArrowAsVisible() {
    cy.getByTestId(selectors['PAGINATION_ARROW_LEFT']).should('be.visible');
  }

  static clickLeftArrow() {
    cy.getByTestId(selectors['PAGINATION_ARROW_LEFT']).click();
  }

  static assertRightArrowAsVisible() {
    cy.getByTestId(selectors['PAGINATION_ARROW_RIGHT']).should('be.visible');
  }

  static clickRightArrow() {
    cy.getByTestId(selectors['PAGINATION_ARROW_RIGHT']).click();
  }

  static assertLeftArrowAsHidden() {
    cy.getByTestId(selectors['PAGINATION_ARROW_LEFT']).should('not.exist');
  }
  static assertRightArrowAsHidden() {
    cy.getByTestId(selectors['PAGINATION_ARROW_RIGHT']).should('not.exist');
  }
}

export default ResultsPaginationFeature;
