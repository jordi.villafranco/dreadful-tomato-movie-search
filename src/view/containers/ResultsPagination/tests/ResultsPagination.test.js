import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import ResultsPagination from '../ResultsPagination';
import { TEST_IDS } from '../constants';
import * as collaborators from '../collaborators';
import store from 'src/state/store';
import location from 'src/utils/location';
import { URL_PARAM_PAGE } from 'src/constants';

const renderResultsPagination = (reduxStore = store) => {
  const spy = jest.spyOn(collaborators, 'calculatePageCount');
  spy.mockReturnValue(4);
  render(
    <Provider store={reduxStore}>
      <ResultsPagination />
    </Provider>
  );
};

describe('Container - ResultsPagination', () => {
  beforeEach(() => {
    renderResultsPagination();
    location.addQuery = jest.fn();
  });
  it('renders the container', () => {
    const wrapper = screen.getByTestId(TEST_IDS.PAGINATION);
    expect(wrapper).toBeInTheDocument();
  });

  it('updates the url search when clicking in a pagination link', () => {
    const indexToClick = 0;
    const link = screen.getAllByTestId(TEST_IDS.PAGINATOR_LINK)[indexToClick];

    fireEvent.click(link);
    expect(location.addQuery).toHaveBeenCalledWith({
      [URL_PARAM_PAGE]: 0
    });
  });
});
