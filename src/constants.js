export const breakpoints = {
  mobile: '(min-width: 320px)',
  tablet: '(min-width: 768px)',
  desktop: '(min-width: 1024px)'
};

export const MOVIES_TYPE = 'movies';
export const SERIES_TYPE = 'series';

export const URL_PARAM_PAGE = 'page';
export const URL_PARAM_TITLE = 'title';
export const URL_PARAM_DATE = 'date';
