import React from 'react';
import { TEST_IDS } from './constants';
import Text from 'src/view/components/Text';
import {
  Wrapper,
  OnHoverContent,
  Footer,
  DateBlock,
  DateIcon,
  DescriptionText
} from './styled';

function Card({ title, description, date, backgroundImage, ...props }) {
  return (
    <Wrapper
      backgroundImage={backgroundImage}
      data-testid={TEST_IDS.CARD}
      tabIndex="0"
      {...props}
    >
      <Footer aria-label={title}>
        <Text as="h3" styling="productive-heading-03" aria-label={title}>
          {title}
        </Text>
        <OnHoverContent data-testid={TEST_IDS.CARD_INFORMATION}>
          <DateBlock>
            <DateIcon icon="calendar" />
            <Text
              as="h4"
              styling="productive-heading-02"
              aria-label={`Year: ${date}`}
            >
              {date}
            </Text>
          </DateBlock>
          <DescriptionText aria-label={`Description: ${description}`}>
            {description}
          </DescriptionText>
        </OnHoverContent>
      </Footer>
    </Wrapper>
  );
}

export default Card;
