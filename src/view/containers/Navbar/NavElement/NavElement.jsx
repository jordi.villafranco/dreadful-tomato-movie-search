import React from 'react';
import { TEST_IDS } from '../constants';

import { StyledNavElement, NavElementText } from './styled';

function NavElement({
  text,
  image,
  imageAlt,
  flexDirection = 'row',
  ...props
}) {
  return (
    <StyledNavElement flexDirection={flexDirection} {...props}>
      <NavElementText data-testid={TEST_IDS.NAVLINK_TEXT}>
        {text}
      </NavElementText>
      <img src={image} data-testid={TEST_IDS.NAVLINK_IMG} alt={imageAlt} />
    </StyledNavElement>
  );
}

export default NavElement;
