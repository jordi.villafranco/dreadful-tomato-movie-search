Feature: User always see the navigation and footer elements on-screen

  As a User I want to see the navigation bar
  so I know that I am in the Dreadful Tomato application

  As a User I want to click the Dreadful Tomato logo
  so I am redirected back to the home page

  As a User I want to see the footer
  so I know that I am in the Dreadful Tomato application

  As a User I want to see the footer
  so I can download the mobile application

  Scenario Outline: User sees the navigation bar
    Given I visit the "<page>" page
    Then I see the navbar

    Examples:
      | page    |
      | landing |
      | series  |
      | movies  |

  Scenario Outline: User sees the Dreadful Tomato logo in the navigation bar
    Given I visit the "<page>" page
    Then I see the navbar
    And I see the company logo inside the navbar

    Examples:
      | page    |
      | landing |
      | series  |
      | movies  |

  Scenario Outline: User is redirected to root when clicking in the navbar logo
    Given I visit the "<page>" page
    And I see the company logo inside the navbar
    When I click in the company logo inside the navbar
    Then I am redirected to the "landing" page

    Examples:
      | page    |
      | landing |
      | series  |
      | movies  |

  Scenario Outline: User sees the footer
    Given I visit the "<page>" page
    Then I see the footer

    Examples:
      | page    |
      | landing |
      | series  |
      | movies  |

  Scenario Outline: User sees the Dreadful Tomato logo in the footer
    Given I visit the "<page>" page
    Then I see the footer
    And I see the company logo inside the footer

    Examples:
      | page    |
      | landing |
      | series  |
      | movies  |

  Scenario Outline: User is redirected to root when clicking in the footer logo
    Given I visit the "<page>" page
    And I see the company logo inside the footer
    When I click in the company logo inside the footer
    Then I am redirected to the "landing" page

    Examples:
      | page    |
      | landing |
      | series  |
      | movies  |

  Scenario Outline: User can see the app badges inside the footer
    Given I visit the "<page>" page
    And I see the footer
    Then I see the footer app badges

    Examples:
      | page    |
      | landing |
      | series  |
      | movies  |

  Scenario Outline: User can see navigation links inside the footer
    Given I visit the "<page>" page
    And I see the footer
    Then I see the footer navigation

    Examples:
      | page    |
      | landing |
      | series  |
      | movies  |

  Scenario Outline: User can see copyright inside the footer
    Given I visit the "<page>" page
    And I see the footer
    Then I see the footer copyright

    Examples:
      | page    |
      | landing |
      | series  |
      | movies  |