Feature: User is able to filter the results in the result page

  As a User I want to filter the results by title
  so I can find the results I'm interested in

  As a User I want to filter the results by date
  so I can find the results I'm interested in

  Scenario Outline: User can filter by title
    Given I visit the "<page>" page
    When I filter the results by the title "<titleFilter>"
    Then I see the expected results for the title filter "<titleFilter>"

    Examples:
      | page   | titleFilter |
      | series | The         |
      | movies | The         |

  Scenario Outline: User can filter by date
    Given I visit the "<page>" page
    When I filter the results by the date "<dateFilter>"
    Then I see the expected results for the date filter "<dateFilter>"

    Examples:
      | page   | dateFilter |
      | series | 2015       |
      | movies | 2014       |



