import { fireEvent, render, screen, within } from '@testing-library/react';
import {
  LOGIN_NAVLINK_TEXT,
  LOGIN_NAVLINK_IMGSRC,
  TEST_IDS,
  TRIAL_TEXT
} from './constants';
import Navbar from './Navbar';

const renderNavbar = (props) => render(<Navbar {...props} />);

describe('Container - Navbar', () => {
  describe('default rendering', () => {
    beforeEach(() => {
      renderNavbar();
    });
    it('renders navbar container', () => {
      const navbar = screen.getByTestId(TEST_IDS.WRAPPER);
      expect(navbar).toBeInTheDocument();
    });

    it('renders the logo', () => {
      const navbar = screen.getByTestId(TEST_IDS.WRAPPER);
      const logo = screen.getByTestId(TEST_IDS.LOGO);
      expect(navbar).toContainElement(logo);
    });

    it('renders the login navigation link', () => {
      const navbar = screen.getByTestId(TEST_IDS.WRAPPER);
      const login = screen.getByTestId(TEST_IDS.LOGIN);
      const text = within(login).getByTestId(TEST_IDS.NAVLINK_TEXT);
      const icon = within(login).getByTestId(TEST_IDS.NAVLINK_IMG);
      expect(navbar).toContainElement(login);
      expect(text.textContent).toContain(LOGIN_NAVLINK_TEXT);
      expect(icon).toHaveAttribute('src', LOGIN_NAVLINK_IMGSRC);
    });

    it('renders the free trial button', () => {
      const navbar = screen.getByTestId(TEST_IDS.WRAPPER);
      const freeTrial = screen.getByTestId(TEST_IDS.FREE_TRIAL);
      const text = within(freeTrial).getByTestId(TEST_IDS.NAVLINK_TEXT);
      expect(navbar).toContainElement(freeTrial);
      expect(text.textContent).toContain(TRIAL_TEXT);
    });

    it('should redirect to the root page when logo link is clicked', () => {
      const logo = screen.getByTestId(TEST_IDS.LOGO);
      expect(logo.closest('a')).toHaveAttribute('href', '/');
    });
  });

  describe('navigation to series and movies', () => {
    beforeEach(() => {
      renderNavbar({ hasNavigation: true });
    });
    it('renders the navigation section when hasNavigation prop is recieved with value true', () => {
      const navbar = screen.getByTestId(TEST_IDS.WRAPPER);
      const navigation = screen.getByTestId(TEST_IDS.NAVIGATION);
      expect(navbar).toContainElement(navigation);
    });
  });

  describe('filters toggler', () => {
    const handleFilterTogglerClickMock = jest.fn();
    beforeEach(() => {
      renderNavbar({
        hasFilterToggler: true,
        isFiltersActive: true,
        handleFilterTogglerClick: handleFilterTogglerClickMock
      });
    });

    it('renders the filters toggler', () => {
      const navbar = screen.getByTestId(TEST_IDS.WRAPPER);
      const filterToggler = screen.getByTestId(TEST_IDS.FILTER_TOGGLER);
      expect(navbar).toContainElement(filterToggler);
    });

    it('fires handleFiltersTogglerClick on onClick event', () => {
      const filterToggler = screen.getByTestId(TEST_IDS.FILTER_TOGGLER);
      fireEvent.click(filterToggler);
      expect(handleFilterTogglerClickMock).toHaveBeenCalled();
    });
  });
});
