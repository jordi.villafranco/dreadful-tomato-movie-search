import { render, screen, fireEvent } from '@testing-library/react';
import Pagination from './Pagination';
import { TEST_IDS } from './constants';

const renderPagination = (props) => render(<Pagination {...props} />);

describe('Container - ResultsList', () => {
  it('renders the component', () => {
    renderPagination();

    const wrapper = screen.getByTestId(TEST_IDS.PAGINATION);
    expect(wrapper).toBeInTheDocument();
  });

  it('renders the amount of links indicated per the count prop', () => {
    const countMock = 5;
    renderPagination({ count: countMock });

    const links = screen.getAllByTestId(TEST_IDS.PAGINATION_LINK);
    expect(links).toHaveLength(countMock);
  });

  it('fires onChange prop when clicking a pagination link', () => {
    const countMock = 5;
    const onChangeMock = jest.fn();
    renderPagination({ count: countMock, onChange: onChangeMock });

    const links = screen.getAllByTestId(TEST_IDS.PAGINATION_LINK);
    links.forEach((link, i) => {
      fireEvent.click(link);
      expect(onChangeMock).toHaveBeenCalledWith(i);
    });
  });

  it('sets pagination link active based on currentPage prop', () => {
    const countMock = 5;
    const currentPageMock = 0;
    renderPagination({ count: countMock, currentPage: currentPageMock });

    const activeLink = screen.getAllByTestId(TEST_IDS.PAGINATION_LINK)[
      currentPageMock
    ];
    expect(activeLink).toBeInTheDocument();
  });

  it('does not render any pagination arrows if count is 0', () => {
    renderPagination({ count: 0 });

    const rightArrow = screen.queryByTestId(TEST_IDS.PAGINATION_ARROW_RIGHT);
    const leftArrow = screen.queryByTestId(TEST_IDS.PAGINATION_ARROW_LEFT);
    expect(rightArrow).not.toBeInTheDocument();
    expect(leftArrow).not.toBeInTheDocument();
  });

  describe('right pagination arrow', () => {
    const currentPageMock = 1;
    const onChangeMock = jest.fn();

    beforeEach(() => {
      renderPagination({
        currentPage: currentPageMock,
        count: 3,
        onChange: onChangeMock
      });
    });

    it('renders currentPage is lesser than count-1', () => {
      const rightArrow = screen.getByTestId(TEST_IDS.PAGINATION_ARROW_RIGHT);
      expect(rightArrow).toBeInTheDocument();
    });

    it('fires onChange prop onClick with the expected parameters', () => {
      const rightArrow = screen.getByTestId(TEST_IDS.PAGINATION_ARROW_RIGHT);
      fireEvent.click(rightArrow);
      expect(onChangeMock).toHaveBeenCalledWith(currentPageMock + 1);
    });
  });

  describe('left pagination arrow', () => {
    const currentPageMock = 1;
    const onChangeMock = jest.fn();
    beforeEach(() => {
      renderPagination({
        currentPage: currentPageMock,
        onChange: onChangeMock
      });
    });

    it('renders if currentPage is greater than 0', () => {
      const leftArrow = screen.getByTestId(TEST_IDS.PAGINATION_ARROW_LEFT);
      expect(leftArrow).toBeInTheDocument();
    });
    it('fires onChange prop onClick with the expected parameters', () => {
      const leftArrow = screen.getByTestId(TEST_IDS.PAGINATION_ARROW_LEFT);
      fireEvent.click(leftArrow);
      expect(onChangeMock).toHaveBeenCalledWith(currentPageMock - 1);
    });
  });
});
