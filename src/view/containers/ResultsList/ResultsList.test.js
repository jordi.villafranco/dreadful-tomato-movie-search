import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import * as redux from 'react-redux';
import ResultsList from './ResultsList';
import { TEST_IDS } from './constants';
import store from 'src/state/store';

const renderResultsList = (reduxStore = store) =>
  render(
    <Provider store={reduxStore}>
      <ResultsList />
    </Provider>
  );

describe('Container - ResultsList', () => {
  it('renders a wrapper container', () => {
    renderResultsList();

    const wrapper = screen.getByTestId(TEST_IDS.WRAPPER);
    expect(wrapper).toBeInTheDocument();
  });

  it('renders cards inside the wrapper based on fetch data', async () => {
    const mockEntry = {
      title: 'test',
      images: {
        'Poster Art': {
          url: ''
        }
      }
    };
    const mockEntries = [mockEntry, mockEntry];
    const spy = jest.spyOn(redux, 'useSelector');
    spy.mockReturnValue(mockEntries);
    renderResultsList();

    const cards = await screen.findAllByTestId(TEST_IDS.CARD);
    expect(cards).toHaveLength(mockEntries.length);
  });
});
