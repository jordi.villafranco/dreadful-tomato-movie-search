import {
  cleanup,
  render,
  screen,
  within,
  fireEvent
} from '@testing-library/react';
import { Provider } from 'react-redux';
import ResultsFilter from './ResultsFilter';
import constants, { TEST_IDS } from './constants';
import location from 'src/utils/location';
import store from 'src/state/store';
import { URL_PARAM_DATE, URL_PARAM_TITLE } from 'src/constants';

const renderComponent = (reduxStore = store) =>
  render(
    <Provider store={reduxStore}>
      <ResultsFilter />
    </Provider>
  );

const renderComponentWithQuery = (query, reduxStore = store) => {
  const mockStore = {
    dispatch: jest.fn(),
    getState: () => ({
      router: {
        location: {
          query
        }
      }
    }),
    subscribe: jest.fn()
  };
  render(
    <Provider store={mockStore}>
      <ResultsFilter />
    </Provider>
  );
};

describe('Container - ResultsFilter', () => {
  afterEach(() => {
    cleanup();
  });

  describe('rendering', () => {
    beforeEach(() => {
      renderComponent();
    });
    it('renders the container', () => {
      const wrapper = screen.getByTestId(TEST_IDS.FILTERS);
      expect(wrapper).toBeInTheDocument();
    });

    it('renders the text input', () => {
      const wrapper = screen.getByTestId(TEST_IDS.FILTERS);
      const input = screen.getByTestId(TEST_IDS.FILTERS_TITLE);
      expect(wrapper).toContainElement(input);
    });

    it('renders the datepicker', () => {
      const wrapper = screen.getByTestId(TEST_IDS.FILTERS);
      const input = screen.getByTestId(TEST_IDS.FILTERS_TITLE);
      const datepicker = screen.getByTestId(TEST_IDS.FILTERS_DATEPICKER);
      expect(wrapper).toBeInTheDocument();
      expect(wrapper).toContainElement(input);
      expect(wrapper).toContainElement(datepicker);
    });

    it('text input shows the correct placeholder', () => {
      const wrapper = screen.getByTestId(TEST_IDS.FILTERS);
      const { queryByPlaceholderText } = within(wrapper);

      const input = queryByPlaceholderText(constants.TITLE_INPUT_PLACEHOLDER);
      expect(input).toBeInTheDocument();
    });

    it('date input shows the correct placeholder', () => {
      const wrapper = screen.getByTestId(TEST_IDS.FILTERS);
      const { queryByPlaceholderText } = within(wrapper);

      const datepicker = queryByPlaceholderText(
        constants.DATEPICKER_PLACEHOLDER
      );
      expect(datepicker).toBeInTheDocument();
    });
  });

  describe('title filter', () => {
    it('text input is prepopulated by title url parameter', () => {
      const filterMock = 'test';
      renderComponentWithQuery({ [URL_PARAM_TITLE]: filterMock });

      const input = screen.getByTestId(TEST_IDS.FILTERS_TITLE);
      expect(input).toHaveValue(filterMock);
    });

    it('updates query on submit', () => {
      location.addQuery = jest.fn();
      const inputValueMock = 'value';
      renderComponent();

      const input = screen.getByTestId(TEST_IDS.FILTERS_TITLE);
      const form = screen.getByTestId(TEST_IDS.FILTERS_TITLE_FORM);
      fireEvent.change(input, { target: { value: inputValueMock } });
      fireEvent.submit(form);
      expect(location.addQuery).toHaveBeenCalledWith({
        [URL_PARAM_TITLE]: inputValueMock
      });
    });
  });

  describe('date filter', () => {
    it('date input is prepopulated by date url parameter', () => {
      const filterMock = '2025';
      renderComponentWithQuery({ [URL_PARAM_DATE]: filterMock });

      const input = screen.getByTestId(TEST_IDS.FILTERS_DATEPICKER);
      expect(input).toHaveValue(filterMock);
    });

    it('updates query on change', () => {
      location.addQuery = jest.fn();
      const inputValueMock = 'date';
      renderComponent();

      const input = screen.getByTestId(TEST_IDS.FILTERS_DATEPICKER);
      fireEvent.change(input, { target: { value: inputValueMock } });
      expect(location.addQuery).toHaveBeenCalledWith({
        [URL_PARAM_DATE]: inputValueMock
      });
    });
  });
});
