import loginIcon from 'src/assets/icon-login.png';
import filterIcon from 'src/assets/icon-filter.png';

export const TEST_IDS = {
  WRAPPER: 'navbar',
  LOGO: 'navbar__logo',
  NAVIGATION: 'navbar__navigation',
  SERIES_NAVLINK: 'navbar__navlink--series',
  MOVIES_NAVLINK: 'navbar__navlink--movies',
  NAVLINK_IMG: 'navlink__image',
  NAVLINK_TEXT: 'navlink__text',
  LOGIN: 'navlink__login',
  FREE_TRIAL: 'navlink__free-trial',
  FILTER_TOGGLER: 'navlink__filter-toggler'
};

export const TRIAL_TEXT = 'Start your free trial';
export const FILTERS_NAVLINK_TEXT = 'Filters';
export const FILTERS_NAVLINK_IMGSRC = filterIcon;

export const LOGIN_NAVLINK_TEXT = 'Login';
export const LOGIN_NAVLINK_IMGSRC = loginIcon;
