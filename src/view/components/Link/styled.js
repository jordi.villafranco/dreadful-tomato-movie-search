import styled from 'styled-components';

const Link = styled.a`
  font-size: 16px;
  line-height: 22px;
  font-weight: 600;
  letter-spacing: 1.3px;
  text-decoration: none;
  color: white;

  &:hover {
    text-decoration: underline;
  }
`;

export default Link;
