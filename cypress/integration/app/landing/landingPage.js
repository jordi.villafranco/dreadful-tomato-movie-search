const selectors = {
  MOVIES: 'landing__section--movies',
  SERIES: 'landing__section--series'
};

const selectorsBySection = {
  movies: selectors['MOVIES'],
  series: selectors['SERIES']
};
class LandingPage {
  static assertSectionAsVisible(section) {
    cy.getByTestId(selectorsBySection[section]).should('be.visible');
  }

  static clickSection(section) {
    cy.getByTestId(selectorsBySection[section]).click();
  }
}

export default LandingPage;
