import React from 'react';
import { useSelector } from 'react-redux';
import { URL_PARAM_PAGE, URL_PARAM_DATE, URL_PARAM_TITLE } from 'src/constants';
import { selectors } from 'src/state/router';
import location from 'src/utils/location';
import TextField from 'src/view/components/TextField';
import Datepicker from 'src/view/components/Datepicker';
import {
  FullWidthWrapper,
  Wrapper,
  TitleInputForm,
  DateInputForm
} from './styled';
import constants, { TEST_IDS } from './constants';

function ResultsFilter({ ...props }) {
  const titleFilter = useSelector(selectors.selectTitleQuery);
  const dateFilter = useSelector(selectors.selectDateQuery);

  const handleSubmitDate = (date) => {
    location.addQuery({ [URL_PARAM_PAGE]: 0 });
    location.addQuery({
      [URL_PARAM_DATE]: typeof date.year === 'function' ? date.year() : date
    });
  };

  const handleSubmitTitle = (e) => {
    e.preventDefault();
    location.addQuery({ [URL_PARAM_PAGE]: 0 });
    location.addQuery({
      [URL_PARAM_TITLE]: e.target.elements[constants.TITLE_INPUT_NAME].value
    });
  };

  return (
    <FullWidthWrapper {...props}>
      <Wrapper data-testid={TEST_IDS.FILTERS}>
        <TitleInputForm
          onSubmit={handleSubmitTitle}
          data-testid={TEST_IDS.FILTERS_TITLE_FORM}
        >
          <TextField
            icon="search"
            name={constants.TITLE_INPUT_NAME}
            data-testid={TEST_IDS.FILTERS_TITLE}
            placeholder={constants.TITLE_INPUT_PLACEHOLDER}
            label="Filter by title"
            defaultValue={titleFilter}
          />
        </TitleInputForm>
        <DateInputForm data-testid={TEST_IDS.FILTERS_DATEPICKER_FORM}>
          <Datepicker
            onChange={handleSubmitDate}
            inputProps={{
              icon: 'calendar',
              name: constants.DATEPICKER_INPUT_NAME,
              'data-testid': TEST_IDS.FILTERS_DATEPICKER,
              placeholder: constants.DATEPICKER_PLACEHOLDER,
              value: dateFilter || '',
              label: 'Filter by date'
            }}
          />
        </DateInputForm>
      </Wrapper>
    </FullWidthWrapper>
  );
}

export default ResultsFilter;
