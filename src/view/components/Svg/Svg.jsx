import React from 'react';
import { TEST_IDS } from './constants';
import { paths } from './paths';
import { StyledSvg } from './styled';

function Svg({ icon, viewbox = '0 0 24 24', ...props }) {
  return (
    <StyledSvg data-testid={TEST_IDS.SVG} viewBox={viewbox} {...props}>
      {paths[icon] && <path d={paths[icon]} data-testid={TEST_IDS.PATH}></path>}
    </StyledSvg>
  );
}

export default Svg;
