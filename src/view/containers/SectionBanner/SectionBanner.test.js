import { render, screen, within, fireEvent } from '@testing-library/react';
import SectionBanner from './SectionBanner';
import constants, { TEST_IDS } from './constants';
import { MOVIES_TYPE, SERIES_TYPE } from 'src/constants';
import { location } from 'src/utils';

const renderSectionBanner = (props) => render(<SectionBanner {...props} />);

describe('Container - Section Banner', () => {
  it.each([
    [SERIES_TYPE, constants.SERIES_BG],
    [MOVIES_TYPE, constants.MOVIES_BG]
  ])(
    'renders the correct background image for %s type',
    (type, backgroundUrl) => {
      renderSectionBanner({ type });
      const wrapper = screen.getByTestId(TEST_IDS.WRAPPER);
      const backgroundDiv = screen.getByTestId(TEST_IDS.BACKGROUND);

      expect(wrapper).toContainElement(backgroundDiv);
      expect(backgroundDiv).toHaveStyle(
        `background-image: url(${backgroundUrl})`
      );
    }
  );

  it.each([
    [SERIES_TYPE, constants.SERIES_TEXT],
    [MOVIES_TYPE, constants.MOVIES_TEXT]
  ])('renders the correct title for %s type', (type, title) => {
    renderSectionBanner({ type });
    const wrapper = screen.getByTestId(TEST_IDS.WRAPPER);
    const titleDiv = screen.getByTestId(TEST_IDS.TITLE);
    const { getByText } = within(titleDiv);

    expect(wrapper).toContainElement(titleDiv);
    expect(getByText(title)).toBeInTheDocument();
  });

  it.each([
    [SERIES_TYPE, constants.SERIES_REDIRECTION],
    [MOVIES_TYPE, constants.MOVIES_REDIRECTION]
  ])(
    'changes href to the corresponding pathname when clicking %s section',
    (type, redirectionPathname) => {
      renderSectionBanner({ type });
      location.push = jest.fn();
      const wrapper = screen.getByTestId(TEST_IDS.WRAPPER);
      fireEvent.click(wrapper);
      expect(location.push).toHaveBeenCalledWith(redirectionPathname);
    }
  );
});
