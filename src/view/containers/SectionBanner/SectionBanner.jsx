import React from 'react';
import constants, { TEST_IDS } from './constants';
import { Background, Wrapper, Footer } from './styled';
import { MOVIES_TYPE, SERIES_TYPE } from 'src/constants';
import Text from 'src/view/components/Text';
import { location } from 'src/utils';

function SectionBanner({ type, ...props }) {
  const backgroundUrlsByType = {
    [SERIES_TYPE]: constants.SERIES_BG,
    [MOVIES_TYPE]: constants.MOVIES_BG
  };

  const textByType = {
    [SERIES_TYPE]: constants.SERIES_TEXT,
    [MOVIES_TYPE]: constants.MOVIES_TEXT
  };

  const pathnameByType = {
    [SERIES_TYPE]: constants.SERIES_REDIRECTION,
    [MOVIES_TYPE]: constants.MOVIES_REDIRECTION
  };

  const handleClick = (e) => {
    e.preventDefault();
    location.push(pathnameByType[type]);
  };

  return (
    <Wrapper
      data-testid={TEST_IDS.WRAPPER}
      onClick={handleClick}
      href={pathnameByType[type]}
      tabIndex="0"
      aria-label={textByType[type]}
      role="button"
      {...props}
    >
      <Background
        data-testid={TEST_IDS.BACKGROUND}
        backgroundImage={backgroundUrlsByType[type]}
      />
      <Footer data-testid={TEST_IDS.TITLE}>
        <Text styling="productive-heading-04">{textByType[type]}</Text>
      </Footer>
    </Wrapper>
  );
}

export default SectionBanner;
