import React from 'react';
import { TEST_IDS } from './constants';
import {
  PaginationLink,
  PaginationWrapper,
  RightPaginationArrow,
  LeftPaginationArrow,
  PaginationArrow
} from './styled';
import Text from 'src/view/components/Text';
import { URL_PARAM_PAGE } from 'src/constants';

function Pagination({ onChange, count, currentPage = 0 }) {
  currentPage = parseInt(currentPage);

  const isPaginationLinkActive = (page) => {
    return page === currentPage;
  };

  const renderPaginationLinks = () => {
    return [...Array(count)].map((_, i) => {
      const pageText = i + 1;
      const page = i;
      return (
        <a
          onClick={(e) => e.preventDefault()}
          aria-label={`Go to page ${pageText}`}
          href={`?${URL_PARAM_PAGE}=${page}`}
          key={`${TEST_IDS.PAGINATION_LINK}-${page}`}
        >
          <PaginationLink
            data-testid={TEST_IDS.PAGINATION_LINK}
            onClick={() => onChange(page)}
            active={isPaginationLinkActive(page)}
          >
            <Text>{pageText}</Text>
          </PaginationLink>
        </a>
      );
    });
  };

  const renderRightArrow = () => {
    const nextPage = currentPage + 1;
    if (currentPage < --count) {
      return (
        <RightPaginationArrow
          as="a"
          href={`?${URL_PARAM_PAGE}=${nextPage}`}
          onClick={(e) => e.preventDefault()}
        >
          <PaginationArrow
            onClick={() => onChange(currentPage + 1)}
            icon="navigateNext"
            aria-label={`Go to next page`}
            data-testid={TEST_IDS.PAGINATION_ARROW_RIGHT}
          />
        </RightPaginationArrow>
      );
    }
  };

  const renderLeftArrow = () => {
    const previousPage = currentPage - 1;
    if (currentPage > 0) {
      return (
        <LeftPaginationArrow
          onClick={(e) => e.preventDefault()}
          as="a"
          href={`?${URL_PARAM_PAGE}=${previousPage}`}
        >
          <PaginationArrow
            onClick={(e) => onChange(previousPage)}
            icon="navigateBefore"
            aria-label={`Go to previous page`}
            data-testid={TEST_IDS.PAGINATION_ARROW_LEFT}
          />
        </LeftPaginationArrow>
      );
    }
  };

  return (
    <PaginationWrapper data-testid={TEST_IDS.PAGINATION}>
      {renderLeftArrow()}
      {renderPaginationLinks()}
      {renderRightArrow()}
    </PaginationWrapper>
  );
}

export default Pagination;
