import styled from 'styled-components';
import Svg from 'src/view/components/Svg';
import Text from 'src/view/components/Text';

export const Footer = styled.div`
  position: absolute;
  bottom: 0;
  max-height: 80px;
  width: 100%;
  padding: 24px;
  padding-left: 16px;
  padding-right: 16px;
  background-color: rgba(0, 0, 0, 0.75);
  transition: max-height 350ms ease;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const OnHoverContent = styled.div`
  flex-direction: column;
  display: none;
`;

export const Wrapper = styled.div`
  cursor: pointer;
  min-width: 180px;
  max-width: 750px;
  min-height: 420px;
  display: block;
  position: relative;
  border-radius: 10px;
  background-image: url(${(props) => props.backgroundImage});
  background-position: center;
  background-size: cover;
  overflow: hidden;

  &:hover ${Footer} {
    max-height: 100%;
    white-space: normal;
  }

  &:hover ${OnHoverContent} {
    display: flex;
  }

  &:focus-within ${Footer} {
    max-height: 100%;
    white-space: normal;
  }

  &:focus-within ${OnHoverContent} {
    display: flex;
  }
`;

export const DateBlock = styled.div`
  display: flex;
  align-items: center;
  margin-top: 8px;
  margin-bottom: 8px;
`;

export const DateIcon = styled(Svg)`
  fill: #ca2b21;
  margin-right: 8px;
`;

export const DescriptionText = styled(Text)`
  color: #bbb;
`;
