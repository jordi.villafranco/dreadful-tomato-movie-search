const selectors = {
  FILTERS: 'filters',
  FILTERS_TITLE: 'filters__input--title',
  FILTERS_TITLE_FORM: 'filters__form--title',
  FILTERS_DATEPICKER: 'filters__input--datepicker',
  FILTERS_DATEPICKER_FORM: 'filters__form--datepicker',
  CARD: 'resultsList__card'
};

class ResultsPage {
  static filterByTitle(title) {
    cy.getByTestId(selectors.FILTERS_TITLE).type(title);
    cy.getByTestId(selectors.FILTERS_TITLE_FORM).submit();
  }

  static filterByDate(date) {
    cy.getByTestId(selectors.FILTERS_DATEPICKER).type(date);
  }

  static assertResultsByTitleFilter(title) {
    cy.url().should('include', `title=${title}`);
  }

  static assertResultsByDateFilter(date) {
    cy.url().should('include', `date=${date}`);
  }
}

export default ResultsPage;
