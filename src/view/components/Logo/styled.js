import styled from 'styled-components';

export const LogoImg = styled.img`
  cursor: pointer;
`;

export const StyledAnchor = styled.a`
  display: flex;
  align-items: center;
`;
