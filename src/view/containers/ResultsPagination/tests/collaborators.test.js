import { calculatePageCount } from '../collaborators';

describe('Testing collaborators', () => {
  describe('Testing calculatePageCount function', () => {
    it('should return 0 when no length is passed as first argument', () => {
      expect(calculatePageCount()).toBe(0);
    });

    it('should return 0 when no resultsPerPage is passed as second argument', () => {
      expect(calculatePageCount(10)).toBe(0);
    });

    it('should return 0 when length is passed as 0', () => {
      expect(calculatePageCount(0, 5)).toBe(0);
    });

    it('should return 1 when length is lesser than resultsPerPage', () => {
      expect(calculatePageCount(5, 10)).toBe(1);
      expect(calculatePageCount(2, 3)).toBe(1);
    });

    it('should return the expected result when length is greater or equal than resultsPerPage', () => {
      expect(calculatePageCount(5, 5)).toBe(1);
      expect(calculatePageCount(100, 10)).toBe(10);
      expect(calculatePageCount(105, 10)).toBe(11);
    });
  });
});
