export const LOCALHOST = 'localhost:3000';

export const PATHNAMES = {
  landing: '', // root pathname will be just /
  movies: 'movies',
  series: 'series'
};
