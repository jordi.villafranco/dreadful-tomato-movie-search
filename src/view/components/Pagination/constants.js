export const TEST_IDS = {
  PAGINATION: 'pagination',
  PAGINATION_LINK: 'pagination__link',
  PAGINATION_LINK_ACTIVE: 'pagination__link--active',
  PAGINATION_ARROW_RIGHT: 'pagination__arrow--right',
  PAGINATION_ARROW_LEFT: 'pagination__arrow--left'
};
